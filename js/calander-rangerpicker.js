
(function($) {

$(window).scroll(function () {
    var $block = $(".GLP_Header_wrap");
    var scrollTop = $(document).scrollTop();
    if (scrollTop >= 10) {
        $block.addClass("GLP_Header_wrap_bg");
        //$block.addClass("bgw");
        if($(".HeaderDatastep1").show()){
            $(".HeaderDatastep1").hide()
            $(".HeaderDatastep1").blur();
        }
        if ($('#nhw_bg').length){
            //判断是否有这个id---是
            $block.removeClass("NoBanner_Header_wrap");
        }

    } else {
        $block.removeClass("GLP_Header_wrap_bg");
        if ($('#nhw_bg').length){
            //判断是否有这个id---是
            $block.addClass("NoBanner_Header_wrap");
            $(".GLP_Header_wrap").removeClass("navToggle_nav_status");
            $(".GLP_Header_wrap").removeClass("Header_book_status");
        }
        //$block.removeClass("bgw");
    }


});

// booknow
/* var isClick = true;
$(".Header_book a").bind("click", function () {
    if (isClick) {
        isClick = false;
        $('.GLP_Header_wrap').toggleClass("BOOKNOW_Header_bg");
    }
    setTimeout(function () {
    isClick = true;
    }, 1);
}); */
// book show
$('#collapseHeaderBOOKNOW').on('show.bs.collapse', function () {
    if(! $(".GLP_Header_wrap").hasClass("BOOKNOW_Header_bg")){
        $('.GLP_Header_wrap').toggleClass("BOOKNOW_Header_bg");
    }
    if($(".GLP_Header_wrap").hasClass("navOpen")){
        $('.GLP_Header_wrap').removeClass("navOpen");
        $('#navToggle').removeClass("expanded");
    }
});
//book hide
$('#collapseHeaderBOOKNOW').on('hide.bs.collapse', function () {
    if($(".GLP_Header_wrap").hasClass("BOOKNOW_Header_bg")){
        setTimeout(function(){
            $('.GLP_Header_wrap').toggleClass("BOOKNOW_Header_bg");
        },200);
    }
});

//booknow and menu
$('#navToggle').click(function(){
    if($(".Header_right_collapse").hasClass("show")){
        $('.Header_right_collapse').removeClass("show");
        $(".GLP_Header_wrap").removeClass("GLP_Header_wrap_bg");
        $(".GLP_Header_wrap").removeClass("BOOKNOW_Header_bg");
    }else{
    }
    if ($('#nhw_bg').length){
        //判断是否有这个id---是
        $(".GLP_Header_wrap").toggleClass("navToggle_nav_status");
    }
    if($("#nav_langdropdown02").hasClass("show")){
        $('#nav_langdropdown02').removeClass("show");
    }
});

//选择房间 number
$('.spinnerRooms').spinner({value:1,min:1,max:9});
$('.spinnerAdults').spinner({value:2,min:1,max:3});
$('.spinnerChild').spinner({value:0,min:0,max:2});
//except the number area, click anywhere could close the collapse
$(function(){
    // $('#collapseNumber div').each(function(){
    //     $(this).attr('collapseNumber','yes');
    // });
    // $('div').click(function(){
    //     if($("#collapseNumber").hasClass("show")){
    //         if($(this).attr('collapseNumber')==='yes')  {
    //             return false;
    //         }
    //         else{
    //             $('#collapseNumber').removeClass("show");
    //         }
    //     }
    // });
    $('#collapseBook a').click(function(){
        if($('#Adults').val() == 3){
            $('#Child').val(0);
            $('#Child').parent('.spinner').find(".decrease").attr("disabled",true).css("pointer-events","none");
            $('#Child').parent('.spinner').find(".increase").attr("disabled",true).css("pointer-events","none");
            //console.log($('#Child').parent('.spinner').find(".decrease"));
        }
        else{
            if ($('#Child').val() < 2) {
                if($('#Child').val()  == 0){
                    $('#Child').parent('.spinner').find(".increase").removeAttr("disabled").css("pointer-events", "auto");
                    $('#Child').parent('.spinner').find(".decrease").attr("disabled", true).css("pointer-events", "none");

                }else{
                    $('#Child').parent('.spinner').find(".increase").removeAttr("disabled").css("pointer-events", "auto");
                    $('#Child').parent('.spinner').find(".decrease").removeAttr("disabled").css("pointer-events", "auto");
                }
               
            }
            
        }

              //人数房间加减
        var content = '';
        var current_language= $("#current_language").val();
        if(current_language == "zh-hans"){
            var room=" 间客房, ";
            var adult=" 位成人, ";
            var child=" 位儿童 ";
        }else if(current_language =="zh-hant"){
            var room=" 客房, ";
            var adult=" 成人, ";
            var child=" 小童 ";
        }else {
            var room=" Room, ";
            var adult=" Adult, ";
            var child=" Child ";
        }
        if($('#Rooms').val() > 1 && current_language == "en")
        {
            content += $('#Rooms').val()+' Rooms, ';
        }
        else{
            content += $('#Rooms').val()+room;
        }

        if($('#Adults').val() > 1  && current_language == "en")
        {
            content += $('#Adults').val()+' Adults, ';
        }
        else{
            content += $('#Adults').val()+adult;
        }

        if($('#Child').val() > 1  && current_language == "en")
        {
            content += $('#Child').val()+' Children ';
        }
        else{
            if($('#Child').val() == 0   && current_language == "en"){
                content += $('#Child').val()+' Child ';
            }else {
                content += $('#Child').val()+child;
            }

        }
        $("#home_content_text1").text(content);
    });
});

//提交
$('#go_booking').click(function(){
    var http_host = $('input[name="http_host"]').val();
    //https://nonememberbe-qa.macausjm-glp.com/reservation/select-room?arrival_date=2020-12-24&departure_date=2020-12-25&rooms=1&adults=2&children=0&promotion_code=&hotel_code=GLPH&session_id=f58ac20c447a11eba5c3005056b3bb8e&group_by=room_type_cate&date=1608658125317
    var url = 'https://'+http_host+'/reservation/select-room?';
    url += 'arrival_date=' + changeDateType($('input[name="arrival_date"]').val());
    url += '&departure_date=' + changeDateType($('input[name="departure_date"]').val());
    url += '&rooms=' + $('#Rooms').val();
    url += '&adults=' + $('#Adults').val();
    url += '&children=' + $('#Child').val();
    var promotion_code = $('#PromotionCode').val();
    if (promotion_code != null && promotion_code != '' && promotion_code!=undefined) {
       url += '&promotion_code='+promotion_code;
   }

   url += '&hotel_code=' + $('#menu_hotelname').attr('data-value');
   url += '&group_by=room_type_cate';
   url += '&lang=' + changeLanguage($("#current_language").val());
   url += '&skin=pvm';
   window.open(url, '_blank');
    // window.location.href = url;
});

var hotelnameChangeCode = function(hotelname){
    if (hotelname.indexOf("Palazzo Versace Macau") >= 0) {
        return 'PVM';
    }else if (hotelname.indexOf("THE KARL LAGERFELD") >= 0) {
        return 'KLH';
    }else{
        return 'GLPH';
    }
}

var changeLanguage = function(language_code){
    if (language_code == 'zh-hant') {
        return 'tc';
    }else if(language_code == 'zh-hans'){
        return 'cn';
    }else{
        return 'en';
    }
}


var changeDateType = function(date){
    // var en_month = ['January','February','March','April','May','June','July','August','September','October','November','December'];
    // var num_month = [1,2,3,4,5,6,7,8,9,10,11,12];
    
    // $.each(en_month,function(i,item){
    //     var month_str = new RegExp(item)
    //     if (month_str.test(date) > 0) {
    //         date = date.replace(' '+item+' ',',');
    //         date = item+''+date;
    //         date = new Date(Date.parse(date));
    //         date = date.Format('yyyy-MM-dd');
    //     }
    // })
    // return date;
    if ($("#current_language").val() != 'en') {
        date = date.match(/\d{4}.\d{1,2}.\d{1,2}/mg).toString();  
        date = date.replace(/[^0-9]/mg, '-');
        var srt = date.split('-'); 
        if (srt[1] < 10) {
            srt[1] = '0'+srt[1];
        }
        if (srt[2] < 10) {
            srt[2] = '0'+srt[2];
        }
        date = srt[0]+'-'+srt[1]+'-'+srt[2];

    }
    return moment(date).format(formatDateStr);

}


Date.prototype.Format = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1,
        "d+": this.getDate(),
        "h+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "q+": Math.floor((this.getMonth() + 3) / 3),
        "S": this.getMilliseconds()
    };
    if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    };




    $('.FormControl_date_select_Number').click(function(){
        if($(".GLP_Header_wrap").hasClass("BOOKNOW_Header_bg")){
            $('.GLP_Header_wrap').addClass("GLP_Header_wrap_bg");
        }

    });

//日历
var formatDateStr='YYYY-MM-DD';
var disabledDate = [];

$(function() {    
    if($("#current_language").val() == 'zh-hant'){
        var month_list=new Array('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月');
        var week_list=['日','一','二','三','四','五','六'];
    }else if($("#current_language").val() == 'zh-hans'){
        var week_list=['日','一','二','三','四','五','六'];
        var month_list=new Array('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月');
    }else{
        var week_list=['Su','Mo','Tu','We','Th','Fr','Sa'];
        var month_list=new Array('January','February','March','April','May','June','July','August','September','October','November','December');
    }
    //初始化
    // $('#datepicker_from').val(fn_formatDate(moment().hours(0).minutes(0).seconds(0).format(formatDateStr)));
    // $('#datepicker_from').val(fn_formatDate(moment('2021-07-15').format(formatDateStr)));
    // $('#datepicker_from1').val(fn_formatDate(moment().hours(0).minutes(0).seconds(0).format(formatDateStr)));
    // $('#datepicker_to').val(fn_formatDate(moment().hours(24).minutes(0).seconds(0).format(formatDateStr)));
    // $('#datepicker_to').val(fn_formatDate(moment('2021-07-16').format(formatDateStr)));
    // $('#datepicker_to1').val(fn_formatDate(moment().hours(24).minutes(0).seconds(0).format(formatDateStr)));
    //触发to
    $('#datepicker_to').click(function(){
        $('#datepicker_from').click();
        $('#datepicker_to').addClass('Top_Booking_inputto');
        $('#datepicker_from').addClass('Top_Booking_inputfrom');
    });
    $('#datepicker_from').click(function(){
        $('#datepicker_from').addClass('Top_Booking_inputfrom');
        $('#datepicker_to').addClass('Top_Booking_inputto');
    });
    
    var minDate = new Date();
    // $('#datepicker_to1').click(function(){
    //     $('#datepicker_from1').click();
    // });
    $('#datepicker_from').daterangepicker({
        // minDate:moment().hours(0).minutes(0).seconds(0), //设置开始日期
        minDate:minDate,
        maxDate:moment().add(1, 'year'),
        format : formatDateStr, //控件中from和to 显示的日期格式
        autoApply: true,
        autoUpdateInput:false,
        alwaysShowCalendars:false,
        SpecialClassname:'HeaderDatastep1',
        locale: {
            daysOfWeek: week_list,
            monthNames: month_list,
        },
        isInvalidDate: function(date) {
            if ($.inArray(date.format('YYYY-MM-DD'), disabledDate) != -1) {
                return true; 
            } else {
                return false; 
            }
            
        }, isCustomDate: function(date) {
            return 'menu_hotel';
        },
    }, function(start, end, label) {
        //格式化日期显示框
        $('#datepicker_from').val(fn_formatDate(start.format(formatDateStr)));
        $('#datepicker_to').val(fn_formatDate(end.format(formatDateStr)));
    });
    // $('#datepicker_from1').daterangepicker({
    //     minDate:moment().hours(0).minutes(0).seconds(0), //设置开始日期
    //     maxDate:moment().add(1, 'year'),
    //     format : formatDateStr, //控件中from和to 显示的日期格式
    //     autoApply: true,
    //     autoUpdateInput:false,
    //     locale: {
    //         // daysOfWeek: week_list,
    //         // monthNames: month_list,
    //     },
    //     isInvalidDate: function(date) {
    //         if ($.inArray(date.format('YYYY-MM-DD'), disabledDate) != -1) {
    //             return true; 
    //         } else {
    //             return false; 
    //         }

    //     },
    // }, function(start, end, label) {
    //     //格式化日期显示框
    //     $('#datepicker_from1').val(fn_formatDate(start.format(formatDateStr)));
    //     $('#datepicker_to1').val(fn_formatDate(end.format(formatDateStr)));
    // });

    var api_host = $('input[name="api_host"]').val();
    // api_host = 'https://wechat.glp.mo:9443/edip/api';
    var api_getUnCheckDate_address = api_host+'/Availability/GetUnCheckDate';
    var api_getFirstAvailDate_address = api_host+'/Availability/GetFirstAvailDate';
    var date_status=true;
    $('.Header_right_book a').click(function() {

        // $('#booking_btn').click(function(){
        if(date_status){
            var hotelCode = $('#menu_hotelname').attr('data-value');
            var DateTo = moment().add(1, 'year').format(formatDateStr);
            var Channel = 'PAP';
            $.ajax({
                type: 'get',
                // url:'https://wechat.glp.mo:9443/edip/api/Availability/GetUnCheckDate',
                url: api_getFirstAvailDate_address,
                dataType: 'json',
                data: {HotelCode: hotelCode, Channel: Channel, RateCodeGroup: '3'},
                success: function (data) {
                    if (data.success === true) {
                        if (data['data']['firstAvailDate'] != null) {
                            var DateFrom = data['data']['firstAvailDate'];
                            minDate = new Date(DateFrom);
                            $('#datepicker_from').data('daterangepicker').setStartDate(minDate);
                            $('#datepicker_from').val(fn_formatDate(moment(DateFrom).format(formatDateStr)));
                            $('#datepicker_to').val(fn_formatDate(moment(DateFrom).hours(24).minutes(0).seconds(0).format(formatDateStr)));
                        } else {
                            $('#datepicker_from').data('daterangepicker').setStartDate(new Date());
                        }
                        date_status=false;
                        $.ajax({
                            type: 'get',
                            url: api_getUnCheckDate_address,
                            dataType: 'json',
                            data: {HotelCode: hotelCode, DateTo: DateTo, Channel: Channel, RateCodeGroup: '3'},
                            success: function (data) {
                                if (data.success === true) {
                                    disabledDate = data['data'];

                                }
                            },
                            error: function (jqXHR) {
                            }
                        });

                    }
                },
                error: function (jqXHR) {
                    //请求失败函数内容
                }
                // });
            });
        }

    });
});

var fn_formatDate=function(str){

    var now = new Date(str);
    var year = now.getFullYear(); 
    var month = now.getMonth();
    var day = now.getDate(); 
    if($("#current_language").val() == 'zh-hant'){
        var enMonth=new Array('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月');
        return year+'年' + enMonth[month] + day+'日';
    }else if($("#current_language").val() == 'zh-hans'){
        var enMonth=new Array('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月');
        return year+'年' + enMonth[month] + day+'日';
    }else{
        var enMonth=new Array('January','February','March','April','May','June','July','August','September','October','November','December');
        return day +' '+ enMonth[month] +' '+ year;
    }
    
};

})(jQuery);

