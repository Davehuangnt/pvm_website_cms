(function($) {

    $(function() {
        $(".Mobile_Totop").goTop();
      });


    $(window).scroll(function() {		
        var windowWidth = $(window).width();
        if(windowWidth > 1200){
            if($(window).scrollTop() >= 240){ 
                // $('.defaultlogo img').addClass('defaultscrolllogo').animate({height:"80px"}, 1);
                $('.defaultlogo').animate({height:"60px"}, 1);
                $('.defaultlogo img').animate({height:"82px"}, 1);
                $('.PVM_Header_logo').css('height','60px');
                $('.PVM_Header_Menu ul li').css('margin','8px 0');
                $('.PVM_Header_right').css('top','22px');
            }else{    
                // $('.defaultlogo img').removeClass('defaultscrolllogo').animate({height:"120px"}, 1);
                $('.defaultlogo').animate({height:"110px"}, 1);
                $('.defaultlogo img').animate({height:"110px"}, 1);
                $('.PVM_Header_logo').css('height','110px');
                $('.PVM_Header_Menu ul li').css('margin','11px 0');
                $('.PVM_Header_right').css('top','57px');
            }  
        }
        if(windowWidth < 1199){
            if($(window).scrollTop() >= 240){ 
                // $('.defaultlogo img').addClass('defaultscrolllogo').animate({height:"80px"}, 1);
                $('.defaultlogo').animate({height:"60px"}, 1);
                $('.defaultlogo img').animate({height:"82px"}, 1);
                $('.PVM_Header_logo').css('height','60px');
            }else{    
                // $('.defaultlogo img').removeClass('defaultscrolllogo').animate({height:"120px"}, 1);
                $('.defaultlogo').animate({height:"60px"}, 1);
                $('.defaultlogo img').animate({height:"60px"}, 1);
                $('.PVM_Header_logo').css('height','60px');
            }  
        }
	});

    
    $(window).scroll(function() {	
        var windowWidth = $(window).width();	
        if(windowWidth > 767){
            if($(window).scrollTop() >= 1){ 
                $('#collapseBook').collapse('hide');
                $('#collapseTopBookingNumber').collapse('hide');
                $('#collapseTopBookingNumber1').collapse('hide');
                $('#collapseTopBookingNumber2').collapse('hide');
                $('#collapseTopBookingNumber3').collapse('hide');
    
            }
        }
	});


    $(document).bind("click",function(e){undefined
        var con_one = $(".Top_Booking_Number");// 设置目标区域
         if(!con_one.is(e.target) && con_one.has(e.target).length === 0){ 
            $("#collapseTopBookingNumber").collapse('hide');//需要隐藏的元素
            $("#collapseTopBookingNumber1").collapse('hide');//需要隐藏的元素
            $("#collapseTopBookingNumber2").collapse('hide');//需要隐藏的元素
            $("#collapseTopBookingNumber3").collapse('hide');//需要隐藏的元素
        }
    });
    
})(jQuery);