(function($) {
  var screenW=document.body.clientWidth;
  if(screenW > 767){
    var bodyH = $(window).height();
    $('#BOOKTABLEModal .modal-dialog').css('margin-top', '175px');
    $(window).resize(function() {
      var bodyH = $(window).height();
      $('#BOOKTABLEModal .modal-dialog').css('margin-top', '175px');
    });
  }
  if(screenW < 992){
    $('.Telephone_row_select2 ul').css('width',document.body.clientWidth - 62);
  }
  var BOOKTABLEModal_title = $('#BOOKTABLEModal_title').text();
  var BOOKTABLEModal_title1 = $('#BOOKTABLEModal_title1').val();
  var BOOKTABLEModal_title2 = $('#BOOKTABLEModal_title2').val();
  var BOOKTABLEModal_title3 = $('#BOOKTABLEModal_title3').val();
  var BOOKTABLEModal_title4 = $('#BOOKTABLEModal_title4').val();

  var current_language = $('#current_language').val();
  var pvm_path=$("#pvm_path").val();
  var guests = 1;
  var max_guests = 8;
  var formatDateStr='YYYY-MM-DD';
  var date_restaurant = [];
  var minDate = new Date();
  var dateone=minDate.format('DD/MM/YYYY');
  var business_hours=[];

  if ($('.business_hours').length){
    var html ="";
    var numtime=0;
    var length=$('.business_hours').length-1;
    if($("#current_language").val() == 'zh-hant'){
      var tips="請選擇您的用餐時間";
    }else if($("#current_language").val() == 'zh-hans'){
      var tips="请选择您的用餐时间";
    }else{

      var tips="Please select your dining time";
    }
    $('.business_hours').each(function(index,element){
      //spa_date.push( $(this).val());
      var business= $(this).val();
      var timelist = business.split(",");
      html +='<div class="form-group row timediv"><label for="MakeAReservationDate" class="timediv_title col-form-label">'+timelist[0]+'</label> <div class="timediv_time"> <div class="collapseReservation_step2_choicetime">';
      var start_time=parseFloat(timelist[1]);
      var end_time=parseFloat(timelist[2]);
      for (start_time;start_time<=end_time;start_time= start_time+0.5){
        var time=parseInt(start_time);
        if(start_time<10){
          if(time == start_time){
            html +='<div class="choicetime_list" id="time'+numtime+'">0'+time+':00</div>';
            var times='0'+time+':00';
            business_hours.push(times);
          }else {
            html +='<div class="choicetime_list" id="time'+numtime+'">0'+time+':30</div>';
            var times='0'+time+':30';
            business_hours.push(times);
          }

        }else {
          if(time == start_time){
            html +='<div class="choicetime_list" id="time'+numtime+'">'+time+':00</div>';
            var times=time+':00';
            business_hours.push(times);
          }else {
            html +='<div class="choicetime_list" id="time'+numtime+'">'+time+':30</div>';
            var times=time+':30';
            business_hours.push(times);
          }
        }
        numtime++;
      }
      html +='</div></div> </div>';

    });
    $("#timeform").append(html);
  }
  if(current_language == 'zh-hant'){
    var month_list=new Array('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月');
    var week_list=['日','一','二','三','四','五','六'];
  }else if(current_language == 'zh-hans'){
    var week_list=['日','一','二','三','四','五','六'];
    var month_list=new Array('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月');
  }else{
    var week_list=['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
    var month_list=new Array('January','February','March','April','May','June','July','August','September','October','November','December');
  }
  var fn_formatDate=function(str){
    var now = new Date(str);
    var year = now.getFullYear();
    var month = now.getMonth();
    var day = now.getDate();
    if($("#current_language").val() == 'zh-hant'){
      var enMonth=new Array('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月');
      return year+'年' + enMonth[month] + day+'日';
    }else if($("#current_language").val() == 'zh-hans'){
      var enMonth=new Array('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月');
      return year+'年' + enMonth[month] + day+'日';
    }else{
      var enMonth=new Array('January','February','March','April','May','June','July','August','September','October','November','December');
      return day +' '+ enMonth[month] +' '+ year;
    }

  };

  //获取可选时段
  function GetTimeSlot(){
    $(".timediv").hide();
    $(".notimesdiv").hide();
    $("#BOOKModal—step2-n").addClass("Unclickable");
    $(".collapseReservation_step2_loading").show();
    var api_host = $('input[name="api_host"]').val();
    var api_GetTimeSlot = api_host + '/TMS/GetTimeSlot';
    var dataajax={
      "shopCode": $("#shopcode").val(),
      "outletCode": $("#outletcode").val(),
      "date":  $("#date_value").val(),
      "adults": $("#adult_value").val(),
      "children": $("#child_value").val()
    };
    for (var m=0;m<business_hours.length;m++){
      $("#time"+m).attr("class","unchoice");
    }
    $.ajax({
      url: pvm_path+"formajax/gettimeajax",
      type: "post",
      data: dataajax,
      success: (response) => {
        var data=response[0].value;
        if(data.length>0){
          $(".notimesdiv").hide();
          var timestatus=0;
          for (var a = 0;a<business_hours.length;a++){
            if ($.inArray(business_hours[a], data) != -1) {
              $("#time"+a).attr("class","choicetime_list");
              timestatus=1
            }
          }
          if(timestatus == 1){
            $(".timediv").show()
            $(".collapseReservation_step2_loading").hide()
          }else {
            $(".timediv").show()
            $(".notimesdiv").show();
            $(".collapseReservation_step2_loading").hide()
          }
          var liobj = $(".choicetime_list");
          liobj.each(function () {
              $(this).bind("click",function(){
                if( $(this).is('.unchoice') === false) {
                  liobj.removeClass("active")
                  $(this).addClass("active");
                  $("#time_value").val($(this).text());
                  $(".timesdiv").hide();
                  $("#BOOKModal—step2-n").removeClass("Unclickable");
                }
              });
          });

        }else {
          $(".notimesdiv").show();
          $(".collapseReservation_step2_loading").hide()
        }
      },
      error: function (data) {
        console.log("An error occured fetching the course browser");
      },
    });
  }

  function GetMonthAvailability(year,month) {
    var dataajax={
      "shopCode": $("#shopcode").val(),
      "outletCode": $("#outletcode").val(),
      "adults": $("#adult_value").val(),
      "children": $("#child_value").val(),
      "year": year,
      "month": month,
    };
    $.ajax({
      url: pvm_path+"formajax/getmonthajax",
      type: "post",
      data: dataajax,
      success: (response) => {
        var data=response[0].value;
        if(data.length>0){
          for (var a = 0;a<data.length;a++){
            date_restaurant.push(data[a]);
          }
        }
      },
      error: function (data) {
        console.log("An error occured fetching the course browser");
      },
    });
  }
  var nowDate = new Date();
  var year = nowDate.getFullYear();
  var month = nowDate.getMonth() + 1 < 10 ? "0" + (nowDate.getMonth() + 1):nowDate.getMonth() + 1;
  var day = nowDate.getDate() < 10 ? "0" + nowDate.getDate() : nowDate.getDate();
  var current_date = year+'-'+month+'-'+day;
  $('#date_value').val(current_date);
  var futuredays=90;
  if($("#futuredays").length >0 ){
        futuredays = parseInt($("#futuredays").val());
    var now = new Date();
    var nowmonth=now.getMonth()+1;
    var nowmonths=now.getFullYear() +'-'+nowmonth;
    var newDatetime = now.setDate(now.getDate() + futuredays);
    var newDate=new Date(newDatetime);
    var newmonth=newDate.getMonth()+1;
    var newmonths=newDate.getFullYear() +'-'+newmonth;
    var data=getMonthBetween(nowmonths,newmonths);
    if(data.length>0){
      //GetMonthAvailability(data);
       // console.log(data);
      for (var i=0;i<data.length;i++){
        var month=data[i].split("-");
        GetMonthAvailability(month[0],month[1]);
      }
    }
  }
  function getMonthBetween(start,end){
    var result = [];
    var s = start.split("-");
    var e = end.split("-");
    var min = new Date();
    var max = new Date();
    min.setFullYear(s[0],s[1]);
    max.setFullYear(e[0],e[1]);
    var curr = min;
    while(curr <= max){
      var month = curr.getMonth();
      //month=month==0?12:month;
      var str=curr.getFullYear()+"-"+(month);
      var s=curr.getFullYear()+"-0";
      if(str==s){
        str=(curr.getFullYear()-1)+"-12";
      }
      result.push(str);
      curr.setMonth(month+1);
    }
    return result;
  }
  $('#MakeAReservationDate1').val(fn_formatDate(nowDate));
  $('#MakeAReservationDate').val(fn_formatDate(nowDate));
  $('#MakeAReservationDate1').daterangepicker({
    minDate:moment().hours(0).minutes(0).seconds(0), //设置开始日期
    maxDate:moment().add(futuredays, 'days'),
    format : formatDateStr,
    singleDatePicker:true,
    autoUpdateInput:false,
    autoApply: true,
    SpecialClassname:'diningDatastep1',
    locale: {
      daysOfWeek: week_list,
      monthNames: month_list,
    },
    startDate: moment().add('days'),
    isInvalidDate: function(date) {
      if(date_restaurant.length>0){
        if ($.inArray(date.format('YYYY-MM-DD'), date_restaurant) != -1) {
          return false;
        } else {
          return true;
        }
      }else {
        //return false;
        return true;
      }
    }
  }, function(start) {
    //格式化日期显示框
    $("#MakeAReservationDate").val(fn_formatDate(start.format(formatDateStr)));
    $('#MakeAReservationDate1').val(fn_formatDate(start.format(formatDateStr)));
    $('#MakeAReservationDate').data('daterangepicker').setStartDate(start.format('MM/DD/YY'));
    date_format=start.format(formatDateStr);
    $("#date_value").val(date_format);
  });
  $('#MakeAReservationDate').daterangepicker({
    minDate:moment().hours(0).minutes(0).seconds(0), //设置开始日期
    maxDate:moment().add(futuredays, 'days'),
    format : formatDateStr,
    singleDatePicker:true,
    autoUpdateInput:false,
    autoApply: true,
    SpecialClassname:'diningDatastep1',
    locale: {
      daysOfWeek: week_list,
      monthNames: month_list,
    },
    startDate: moment().add('days'),
    parentEl: "#BOOKTABLEModal",
    isInvalidDate: function(date) {
      if(date_restaurant.length>0){
        if ($.inArray(date.format('YYYY-MM-DD'), date_restaurant) != -1) {
          return false;
        } else {
          return true;
        }
      }else {
        //return false;
        return true;
      }

    }
  }, function(start) {
    //格式化日期显示框
    $("#MakeAReservationDate").val(fn_formatDate(start.format(formatDateStr)));
    $('#MakeAReservationDate1').val(fn_formatDate(start.format(formatDateStr)));
    $('#MakeAReservationDate1').data('daterangepicker').setStartDate(start.format('MM/DD/YY'));
    date_format=start.format(formatDateStr);
    $("#date_value").val(date_format);
    GetTimeSlot();
  });




  $(".fnjian").click(function(){
    var min = 1;
    var _this=$('.Adults');
    //var val=_this.val().replace(' Adults','').replace(' Adult','').replace(' 成人','');
    var val=$("#adult_value").val();
    if(val>min)	{
      val=parseInt (val)-1;
    }
    $('.Error_Tips_00').hide();
    $("#adult_value").val(val);
    _this.val(val);
  /*  if(current_language == 'en'){
      if(val<=1)
      {
        _this.val(val+' '+'Adult');
      }
      else
      {
        _this.val(val+' '+'Adults');
      }
    }else{
      _this.val(val+' '+'成人');
    }*/
  });

  $(".fnjia").click(function(){
    var _this=$('.Adults');
    //var adult=_this.val().replace(' Adults','').replace(' Adult','').replace(' 成人','');
    //var child=$('.Child').val().replace(' Children','').replace(' Child','').replace(' 儿童','').replace(' 小童','');
    var adult=$("#adult_value").val();
    var child=$("#child_value").val();
    guests = parseInt(adult)+parseInt(child);
    if(guests < max_guests)	{
      adult=parseInt (adult)+1;
      $("#adult_value").val(adult);
    }else{
      $('.Error_Tips_00').show();
      return false;
    }
    _this.val(adult);
/*    if(current_language == 'en'){
      if(adult<=1)
      {
        _this.val(adult+' '+'Adult');
      }
      else
      {
        _this.val(adult+' '+'Adults');
      }
    }else{
      _this.val(adult+' '+'成人');
    }*/

  });

  $(".fnjian1").click(function(){
    var min = 0;
    var _this=$('.Child');
    //var val=_this.val().replace(' Children','').replace(' Child','').replace(' 儿童','').replace(' 小童','');
    var val=$("#child_value").val();
    if(val>min)	{
      val=parseInt (val)-1;
      $("#child_value").val(val);
    }
    $('.Error_Tips_00').hide();
    _this.val(val);
/*    if(current_language == 'zh-hans'){
      _this.val(val+' '+'儿童');
    }else if(current_language == 'zh-hant'){
      _this.val(val+' '+'小童');
    }else{
      if(val<=1)
      {
        _this.val(val+' '+'Child');
      }
      else
      {
        _this.val(val+' '+'Children');
      }
    }*/

  });

  $(".fnjia1").click(function(){
    var _this=$('.Child');
    //var adult=$('.Adults').val().replace(' Adults','').replace(' Adult','').replace(' 成人','');
    //var child=_this.val().replace(' Children','').replace(' Child','').replace(' 儿童','').replace(' 小童','');
    var adult=$("#adult_value").val();
    var child=$("#child_value").val();
    guests = parseInt(adult)+parseInt(child);
    if(guests<max_guests)	{
      child=parseInt (child)+1;
      $("#child_value").val(child);
    }else{
      $('.Error_Tips_00').show();
      return false;
    }
    _this.val(child);
  /*  if(current_language == 'zh-hans'){
      _this.val(child+' '+'儿童');
    }else if(current_language == 'zh-hant'){
      _this.val(child+' '+'小童');
    }else{
      if(child<=1)
      {
        _this.val(child+' '+'Child');
      }
      else
      {
        _this.val(child+' '+'Children');
      }
    }*/
  });

  $('#carouselExampleCaptions2').carousel({
    interval:false,
    pause:"hover"
  });
  $(".Captcha_refresh").on("click",function () {

    var pvm_path=$("#pvm_path").val();
    var url = pvm_path+"formajax/vcode?tmp="+Math.random();
    $("#code_images").attr('src',url);
  });
  function ResetForm(){
    var url = pvm_path+"formajax/vcode?tmp="+Math.random();
    $("#code_images").attr('src',url);
    $(".Errors_Tips").hide();
    $("#title").val('');
    if($("#current_language").val() == 'zh-hant'){
      $(".select_word01").text('-請選擇-');
    }else if($("#current_language").val() == 'zh-hans'){
      $(".select_word01").text('-请选择-');
    }else{
      $(".select_word01").text('-Please Select-');
    }
    $("#FirstName").val('');
    $("#TelephoneN").val('');
    $("#LastName").val('');
    $("#Email").val('');
    $("#Captcha").val('');
    $("#Message").val('');
    $('#date_value').val(current_date);
    $('#MakeAReservationDate1').val(fn_formatDate(nowDate));
    $('#MakeAReservationDate').val(fn_formatDate(nowDate));
    $('#adult_value').val(1);
    $('#child_value').val(0);
    $('#time_value').val('');
  }
  $(function() {
    $('#BOOKModal—step1-n').click(function(){
      if($(".Errors_sys_Tips").length > 0) {
        return  false;
      }else {
        $("#BookForm01").hide();
        $("#BookForm02").show();
        GetTimeSlot();
        $("#BOOKTABLEModal_title").html(BOOKTABLEModal_title1);
      }
    });
    $('#BOOKModal—step2-p').click(function(){
      $("#BookForm01").show();
      $("#BookForm02").hide();
      $("#BOOKTABLEModal_title").html(BOOKTABLEModal_title);
    });
    $('#BOOKModal—step2-n').click(function(){
      var html="";
      var times=$("#time_value").val();
      if( times.length == 0 || times == ""){
        $(".timesdiv").show();
        return false;
      }
      var child=$("#child_value").val();
      var adults=$("#adult_value").val();
      var dates=$("#MakeAReservationDate1").val();
      //var times=$("#time_value").val();
      if($("#current_language").val() == 'zh-hant'){
        html ="<p>用餐日期：</b> "+dates+"</p><p>用餐時間："+times+"</p><p>賓客人數："+adults+" 成人, "+child+" 儿童</p>";
      }else if($("#current_language").val() == 'zh-hans'){
        html ="<p>用餐日期："+dates+"</p><p>用餐时间： "+times+"</p><p>宾客人数："+adults+" 成人, "+child+" 儿童</p>";
      }else{
        if(parseInt (child) >0){
          var child_name=" Children";
        }  else{
          var child_name=" Child";
        }
        html ="<p>Date:  "+dates+"</p><p>Time: "+times+"</p><p>Guests: "+adults+" Adults, "+child+" "+child_name+" </p>";
      }
      $(".BookForm04_detail").html(html);
      $("#BookForm04").show();
      $("#BookForm02").hide();
      $("#BOOKTABLEModal_title").html(BOOKTABLEModal_title2);
    });

    $('#BOOKModal—step4-p').click(function(){
      $("#BookForm02").show();
      $("#BookForm04").hide();
      $("#BOOKTABLEModal_title").html(BOOKTABLEModal_title1);
    });
    var status=true;
    $('#BOOKModal—step4-n').click(function(){
      var submit=true;
      var arr = ['-Please Select-', '-請選擇-', '-请选择-'];
      $(".Errors_Tips").show();
      if ($('#titlesel').val()==='') {
        $(".Error_Tips0").show();
        submit = false;
      }



      if($('#FirstName').val()===''){
        $(".Error_Tips1").show();
        submit = false;
      }
      if($('#LastName').val()===''){
        $(".Error_Tips2").show();
        submit = false;
      }
      if($('#FirstName').val()!=='' && $('#LastName').val()!==''){
        $(".Error_Tips1").hide();
        $(".Error_Tips2").hide();
      }
      if($('#TelephoneN').val()===''){
        if($("#current_language").val() == 'zh-hant'){
          $(".Error_Tips3").find('.Errors_Tips').text('請填寫您的電話號碼');
        }else if($("#current_language").val() == 'zh-hans'){
          $(".Error_Tips3").find('.Errors_Tips').text('请填写您的电话号码');
        }else{
          $(".Error_Tips3").find('.Errors_Tips').text('Please input your phone no.');
        }
        $(".Error_Tips3").show();
        submit = false;
      }else{
        var reg01 = /^[0-9-]{7,15}$/;
        if (!reg01.test($('#TelephoneN').val())) {
          if($("#current_language").val() == 'zh-hant'){
            $(".Error_Tips3").find('.Errors_Tips').text('電話號碼介乎7至15位數字');
          }else if($("#current_language").val() == 'zh-hans'){
            $(".Error_Tips3").find('.Errors_Tips').text('电话号码介乎7至15位数字');
          }else{
            $(".Error_Tips3").find('.Errors_Tips').text('Between 7 and 15 characters');
          }
          $(".Error_Tips3").show();
          submit = false;
        }
      }

      /*
      if($('#Email').val()===''){
        $(".Error_Tips4").show();
        submit = false;
      }
      */
      var $email_num = $("#Email").val();
      var reg01 = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/; //
      if($email_num.length>0) {
        if (reg01.test($email_num)) {

        } else {
          $(".Error_Tips4").show();
          submit = false;
        }
      }
      if ($('#Captcha').val() === '') {
        $(".Error_Tips5").show();
        submit = false;
      }
      // if($("#check-me").is(':checked')){
      //   $(".Error_Tips6").hide();
      // }else {
      //   $(".Error_Tips6").show();
      //   status=false;
      // }
      if($("#check-me").is(':checked')){
        $(".Error_Tips6").hide();
      }else {
        $(".Error_Tips6").show();
        submit=false;
      }
      if(submit && status){
        submit=false;
        status=false;
        var formdata = $('#restaurantform').serialize();
        formdata= formdata +"&current_language="+ $("#current_language").val();
        //console.log(formdata);
        $.ajax({
          url: pvm_path+"formajax/restaurantajax",
          type: "post",
          data: formdata,
          success: (response) => {
            var data=response[0].value;
            if(data.code === 1000){
              var confirm_no=data.confirm_no;
              if(confirm_no.length >0){
                var current_language= $("#current_language").val();
                var html = '';

                var child=$(".Child").val();
                var adults=$(".Adults").val();
                var dates=$("#MakeAReservationDate").val();
                var times=$("#time_value").val();

                if($("#current_language").val() == 'zh-hant'){
                  html +='<div class="BookForm05_Book_info">';
                  html +='<div class="BookForm05_Book_info_left">預訂編號：</div>';
                  html +='<div class="BookForm05_Book_info_right">'+data.confirm_no+'</div>';
                  html +="</div>";
                  html +='<div class="BookForm05_Book_info">';
                  html +='<div class="BookForm05_Book_info_left">餐廳：</div>';
                  html +='<div class="BookForm05_Book_info_right">'+$("#Restaurant").val()+'</div>';
                  html +="</div>";
                  html +='<div class="BookForm05_Book_info">';
                  html +='<div class="BookForm05_Book_info_left">用餐日期： </div>';
                  html +='<div class="BookForm05_Book_info_right">'+dates+'</div>';
                  html +="</div>";
                  html +='<div class="BookForm05_Book_info">';
                  html +='<div class="BookForm05_Book_info_left">用餐時間：</div>';
                  html +='<div class="BookForm05_Book_info_right">'+times+'</div>';
                  html +="</div>";
                  html +='<div class="BookForm05_Book_info">';
                  html +='<div class="BookForm05_Book_info_left">賓客人數： </div>';
                  html +='<div class="BookForm05_Book_info_right">'+adults+' 成人,  '+child+' 儿童</div>';
                  html +="</div>";
                }else if($("#current_language").val() == 'zh-hans'){
                  html +='<div class="BookForm05_Book_info">';
                  html +='<div class="BookForm05_Book_info_left">预定编号：</div>';
                  html +='<div class="BookForm05_Book_info_right">'+data.confirm_no+'</div>';
                  html +="</div>";
                  html +='<div class="BookForm05_Book_info">';
                  html +='<div class="BookForm05_Book_info_left">餐厅：</div>';
                  html +='<div class="BookForm05_Book_info_right">'+$("#Restaurant").val()+'</div>';
                  html +="</div>";
                  html +='<div class="BookForm05_Book_info">';
                  html +='<div class="BookForm05_Book_info_left">用餐日期：</div>';
                  html +='<div class="BookForm05_Book_info_right">'+dates+'</div>';
                  html +="</div>";
                  html +='<div class="BookForm05_Book_info">';
                  html +='<div class="BookForm05_Book_info_left">用餐时间：</div>';
                  html +='<div class="BookForm05_Book_info_right">18:00</div>';
                  html +="</div>";
                  html +='<div class="BookForm05_Book_info">';
                  html +='<div class="BookForm05_Book_info_left">宾客人数：</div>';
                  html +='<div class="BookForm05_Book_info_right">'+adults+' 成人,  '+child+' 小童</div>';
                  html +="</div>";
                }else{
                  html +='<div class="BookForm05_Book_info">';
                  html +='<div class="BookForm05_Book_info_left">Confirmation Number:</div>';
                  html +='<div class="BookForm05_Book_info_right">'+data.confirm_no+'</div>';
                  html +="</div>";
                  html +='<div class="BookForm05_Book_info">';
                  html +='<div class="BookForm05_Book_info_left">Restaurant:</div>';
                  html +='<div class="BookForm05_Book_info_right">'+$("#Restaurant").val()+'</div>';
                  html +="</div>";
                  html +='<div class="BookForm05_Book_info">';
                  html +='<div class="BookForm05_Book_info_left">Date:</div>';
                  html +='<div class="BookForm05_Book_info_right">'+dates+'</div>';
                  html +="</div>";
                  html +='<div class="BookForm05_Book_info">';
                  html +='<div class="BookForm05_Book_info_left">Time:</div>';
                  html +='<div class="BookForm05_Book_info_right">18:00</div>';
                  html +="</div>";
                  html +='<div class="BookForm05_Book_info">';
                  html +='<div class="BookForm05_Book_info_left">Guest:</div>';
                  html +='<div class="BookForm05_Book_info_right">'+adults+' Adults,  '+child+' Chindren</div>';
                  html +="</div>";
                }
                $(".BookForm05_Book").html(html);
                $("#BookForm04").hide();
                $("#BookForm05").show();
                $('#BOOKTABLEModal h5#BOOKTABLEModal_title').hide();
                $("#BOOKTABLEModal_title").html(BOOKTABLEModal_title3);
              }else {
                $("#BookForm04").hide();
                $("#BookForm06").show();
                $("#BOOKTABLEModal_title").html(BOOKTABLEModal_title4);
              }
              //重置表单
              var url = pvm_path+"formajax/vcode?tmp="+Math.random();
              $("#code_images").attr('src',url);
              $(".Errors_Tips").hide();
              $("#titlesel").val('');
              if($("#current_language").val() == 'zh-hant'){
                $(".select_word01").text('-請選擇-');
                $("#titlesel00").text('-請選擇-');
                $(".Adults").val('1');
                $(".Child").val('0');
              }else if($("#current_language").val() == 'zh-hans'){
                $(".select_word01").text('-请选择-');
                $("#titlesel00").text('-请选择-');
                $(".Adults").val('1');
                $(".Child").val('0');
              }else{
                $(".select_word01").text('-Please Select-');
                $("#titlesel00").text('-Please Select-');
                $(".Adults").val('1');
                $(".Child").val('0');
              }
              $("#BOOKModal—step2-n").addClass("Unclickable");
              $("#FirstName").val('');
              $("#TelephoneN").val('');
              $("#LastName").val('');
              $("#Email").val('');
              $("#adult_value").val(1);
              $("#child_value").val(0);
              $("#Captcha").val('');
              $("#Message").val('');
              $("#check-me").prop("checked",false);
            }else {
              $(".Error_Tips5").show();
            }
            setTimeout(function(){
              submit=true;
              status=true;
            },500);
          },
          error: function (data) {
            setTimeout(function(){
              submit=true;
              status=true;
            },500);
            console.log("An error occured fetching the course browser");
          },
        });
      }

      //$('#BOOKTABLEModal h5#BOOKTABLEModal_title').hide();
    });
    $("#titlesel00").click(function(){
      $(".Error_Tips0").hide();
    });
    $("#FirstName").focus(function(){
      $(".Error_Tips1").hide();
    });
    $("#LastName").focus(function(){
      $(".Error_Tips2").hide();
    });
    $("#TelephoneN").focus(function(){
      $(".Error_Tips3").hide();
    });
    $("#Email").focus(function(){
      $(".Error_Tips4").hide();

    });
    $("#Captcha").focus(function(){
      $(".Error_Tips5").hide();
    });

    $("#FirstName").focus(function(){
      $(".Error_Tips0").hide();
      $(".Error_Tips1").hide();
    });
    $("#LastName").focus(function(){
      $(".Error_Tips0").hide();
      $(".Error_Tips2").hide();
    });
    $("#TelephoneN").focus(function(){
      $(".Error_Tips3").hide();
    });
    $("#Email").focus(function(){
      $(".Error_Tips4").hide();
    });
    $("#check-me").click(function(){
      $(".Error_Tips6").hide();
    });
    $('#BOOKModal—step5-o').click(function(){
      $('#BOOKTABLEModal').modal('hide');
      $("#titlesel00").removeClass('new_select_txt');
      setTimeout(function() {
        ResetForm();
        $("#BookForm01").show();
        $("#BookForm05").hide();
      }, 500);
      $('#BOOKTABLEModal h5#BOOKTABLEModal_title').show();
    });

    $(".close").click(function () {
      var url = pvm_path+"formajax/vcode?tmp="+Math.random();
      $("#code_images").attr('src',url);
      $(".Errors_Tips").hide();
      $("#title").val('');
      if($("#current_language").val() == 'zh-hant'){
        $(".select_word01").text('-請選擇-');
        $("#titlesel00").text('-請選擇-');
        $(".Adults").val('1');
        $(".Child").val('0');
      }else if($("#current_language").val() == 'zh-hans'){
        $(".select_word01").text('-请选择-');
        $("#titlesel00").text('-请选择-');
        $(".Adults").val('1');
        $(".Child").val('0');
      }else{
        $(".select_word01").text('-Please Select-');
        $("#titlesel00").text('-Please Select-');
        $(".Adults").val('1');
        $(".Child").val('0');
      }
      $("#BOOKModal—step2-n").addClass("Unclickable");
      $("#FirstName").val('');
      $("#TelephoneN").val('');
      $("#LastName").val('');
      $("#Email").val('');
      $("#Captcha").val('');
      $("#Message").val('');
      ResetForm();
      $("#BookForm02").hide();
      $("#BookForm04").hide();
      $("#BookForm05").hide();
      $("#BookForm06").hide();
      $("#BookForm01").show();
    });

  });

  //下拉
  $('.Telephone_row_area [name="nice-select"]').click(function(e){
    $(this).find('ul').toggle();
    e.stopPropagation();
    $('.Telephone_row_area').toggleClass('FormControl_dropdownHotel_icon');
  });
  $('.Telephone_row_area [name="nice-select"] li').hover(function(e){
    $(this).toggleClass('on');
    e.stopPropagation();
  });
  $('.Telephone_row_area [name="nice-select"] li').click(function(e){
    var val = $(this).text();
    $(this).parents('.Telephone_row_area [name="nice-select"]').find('input').val(val);
    $('.Telephone_row_area [name="nice-select"] ul').hide();
    e.stopPropagation();
    $('.Telephone_row_area').removeClass('FormControl_dropdownHotel_icon');
  });
  $(document).click(function(){
    $('.Telephone_row_area [name="nice-select"] ul').hide();
    $('.Telephone_row_area').removeClass('FormControl_dropdownHotel_icon');
  });
  $('#booNavigation').click(function(){
    $('.GLP_Header_wrap').removeClass('Header_Menu_scroll_Class');
  });


  //下拉
  function diy_select(){this.init.apply(this,arguments)};
  diy_select.prototype={
     init:function(opt)
     {
       this.setOpts(opt);
       this.o=this.getByClass(this.opt.TTContainer,document,'div');//容器
       this.b=this.getByClass(this.opt.TTDiy_select_btn);//按钮
       this.t=this.getByClass(this.opt.TTDiy_select_txt);//显示
       this.l=this.getByClass(this.opt.TTDiv_select_list);//列表容器
       this.ipt=this.getByClass(this.opt.TTDiy_select_input);//列表容器
       this.lengths=this.o.length;
       this.showSelect();
     },
     addClass:function(o,s)//添加class
     {
       o.className = o.className ? o.className+' '+s:s;
     },
     removeClass:function(o,st)//删除class
     {
       var reg=new RegExp('\\b'+st+'\\b');
       o.className=o.className ? o.className.replace(reg,''):'';
     },
     addEvent:function(o,t,fn)//注册事件
     {
       return o.addEventListener ? o.addEventListener(t,fn,false):o.attachEvent('on'+t,fn);
     },
     showSelect:function()//显示下拉框列表
     {
       var This=this;
       var iNow=0;
       this.addEvent(document,'click',function(){
          for(var i=0;i<This.lengths;i++)
          {
            This.l[i].style.display='none';
          }
       })
       for(var i=0;i<this.lengths;i++)
       {
         this.l[i].index=this.b[i].index=this.t[i].index=i;
         this.t[i].onclick=this.b[i].onclick=function(ev)  
         {
           var e=window.event || ev;
           var index=this.index;
           This.item=This.l[index].getElementsByTagName('li');

           This.l[index].style.display= This.l[index].style.display=='block' ? 'none' :'block';
           for(var j=0;j<This.lengths;j++)
           {
             if(j!=index)
             {
               This.l[j].style.display='none';
             }
           }
           This.addClick(This.item);
           e.stopPropagation ? e.stopPropagation() : (e.cancelBubble=true); //阻止冒泡
         }
       }
     },
     addClick:function(o)//点击回调函数
     {

       if(o.length>0)
       {
         var This=this;
         for(var i=0;i<o.length;i++)
         {
           o[i].onmouseover=function()
           {
             This.addClass(this,This.opt.TTFcous);
           }
           o[i].onmouseout=function()
           {
             This.removeClass(this,This.opt.TTFcous);
           }
           o[i].onclick=function()
           {
             var index=this.parentNode.index;//获得列表
             This.t[index].innerHTML=This.ipt[index].value=this.innerHTML.replace(/^\s+/,'').replace(/\s+&/,'');
             This.l[index].style.display='none';
             var id =This.ipt[index].id;
             var type=this.getAttribute("data-value");
             if(id == "spatype"){
               $(".Error_Tips_spatype").hide();
               $("#spaname").val('');
               $(".Reservation_Error_tips_spatype").hide();
               if($("#current_language").val() == 'zh-hant'){
                 $("#spaname_text").html('-請選擇療程-');
               }else if($("#current_language").val() == 'zh-hans'){
                 $("#spaname_text").html('-请选择疗程-');
               }else{
                 $("#spaname_text").html('-Please select the treatment-');
               }
               if($('.spaname_info_'+type).length){
                 var html2=""
                 var num2=1;
                 $('.spaname_info_'+type).each(function(){
                   var spaname= $(this).val();
                   if(spaname.indexOf("/") != -1){
                     var spanamelist = spaname.split("/");
                     if(num2 == 1){
                       // $("#spaname").val(spanamelist[1]);
                     }
                     html2 += ' <li data-value="'+spanamelist[0]+'">'+spanamelist[1]+'</li>';
                     num2++;
                   }
                 });
                 $("#spaname_ul").html(html2);
               }
             }else if(id == "spaname"){
               $(".Error_Tips_spaname").hide();
             }else if(id == "titlesel"){
               $(".Error_Tips_Title").hide();
             }else if(id == "time"){
               $(".Error_Tips5").hide();
             }
           }
         }
       }
     },
     getByClass:function(s,p,t)//使用class获取元素
     {
       var reg=new RegExp('\\b'+s+'\\b');
       var aResult=[];
       var aElement=(p||document).getElementsByTagName(t || '*');

       for(var i=0;i<aElement.length;i++)
       {
         if(reg.test(aElement[i].className))
         {
           aResult.push(aElement[i])
         }
       }
       return aResult;
     },

     setOpts:function(opt) //以下参数可以不设置  //设置参数
     { 
       this.opt={
          TTContainer:'diy_select',//控件的class
          TTDiy_select_input:'diy_select_input',//用于提交表单的class
          TTDiy_select_txt:'diy_select_txt',//diy_select用于显示当前选中内容的容器class
          TTDiy_select_btn:'diy_select_btn',//diy_select的打开按钮
          TTDiv_select_list:'diy_select_list',//要显示的下拉框内容列表class
          TTFcous:'focus'//得到焦点时的class
       }
       for(var a in opt)  //赋值 ,请保持正确,没有准确判断的
       {
         this.opt[a]=opt[a] ? opt[a]:this.opt[a];
       }
     }
  }


  var TTDiy_select=new diy_select({  //参数可选
  TTContainer:'diy_select',//控件的class
  TTDiy_select_input:'diy_select_input',//用于提交表单的class
  TTDiy_select_txt:'diy_select_txt',//diy_select用于显示当前选中内容的容器class
  TTDiy_select_btn:'diy_select_btn',//diy_select的打开按钮
  TTDiv_select_list:'diy_select_list',//要显示的下拉框内容列表class
  TTFcous:'focus'//得到焦点时的class
  });//如同时使用多个时请保持各class一致.
  $('#titlesel_ul li').click(function(){
    $("#titlesel00").addClass('new_select_txt');
});






})(jQuery);
