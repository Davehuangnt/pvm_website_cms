(function($) {
$(function(){
	var disabledDate = [];
	var api_host = $('input[name="api_host"]').val();
    var api_getUnCheckDate_address = api_host+'/Availability/GetUnCheckDate';
    var api_getFirstAvailDate_address = api_host+'/Availability/GetFirstAvailDate';
	$('.Mobile_booking a').on('click',function () {
		// if ($('input[name="mobile_arrival_date"]').val() == '') {
		// 	// $('input[name="mobile_arrival_date"]').val(fn_formatDate(moment().format('YYYY-MM-DD')));
		// 	$('input[name="mobile_arrival_date"]').val(fn_formatDate(moment('2021-07-15').format('YYYY-MM-DD')));
		// }
		// if ($('input[name="mobile_departure_date"]').val() == '') {
		// 	// $('input[name="mobile_departure_date"]').val(fn_formatDate(moment().add(1, 'days').format('YYYY-MM-DD')));
		// 	$('input[name="mobile_departure_date"]').val(fn_formatDate(moment('2021-07-16').format('YYYY-MM-DD')));
		// }
		var hotelCode = $('#mobile_booking_hotel').attr('data-value');
        var DateTo = moment().add(1, 'year').format('YYYY-MM-DD');
        var Channel = 'PAP';
        $.ajax({
        type:'get',
        url:api_getFirstAvailDate_address,
        dataType:'json',
        data:{HotelCode:hotelCode,Channel:Channel,RateCodeGroup:'3'},
        success:function(data){
            if(data.success === true)
            {
            	var DateFrom = '';
                if (data['data']['firstAvailDate'] != null) {
                DateFrom = data['data']['firstAvailDate'];
                $('input[name="mobile_arrival_date"]').val(fn_formatDate(moment(DateFrom).format('YYYY-MM-DD')));
                $('input[name="mobile_departure_date"]').val(fn_formatDate(moment(DateFrom).hours(24).minutes(0).seconds(0).format('YYYY-MM-DD')));
                }
                showMonthCalendar();
                $.ajax({
		        type:'get',
		        url:api_getUnCheckDate_address,
		        dataType:'json',
		        data:{HotelCode:hotelCode,DateTo:DateTo,Channel:Channel,RateCodeGroup:'3'},
		        success:function(data){
		            if(data.success === true)
		            {
		                disabledDate = data['data'];
		               
		                if (DateFrom != '') {
		                	hideMonthCalendar(DateFrom);
		                }
		            }
		        },
		    	error:function(jqXHR){
		        	}
		    	});
            }
        },
    	error:function(jqXHR){
        	}
    	});
	});
    $('.Tablet_booking a').on('click',function () {
        // if ($('input[name="mobile_arrival_date"]').val() == '') {
        // 	// $('input[name="mobile_arrival_date"]').val(fn_formatDate(moment().format('YYYY-MM-DD')));
        // 	$('input[name="mobile_arrival_date"]').val(fn_formatDate(moment('2021-07-15').format('YYYY-MM-DD')));
        // }
        // if ($('input[name="mobile_departure_date"]').val() == '') {
        // 	// $('input[name="mobile_departure_date"]').val(fn_formatDate(moment().add(1, 'days').format('YYYY-MM-DD')));
        // 	$('input[name="mobile_departure_date"]').val(fn_formatDate(moment('2021-07-16').format('YYYY-MM-DD')));
        // }
        var hotelCode = $('#mobile_booking_hotel').attr('data-value');
        var DateTo = moment().add(1, 'year').format('YYYY-MM-DD');
        var Channel = 'PAP';
        $.ajax({
            type:'get',
            url:api_getFirstAvailDate_address,
            dataType:'json',
            data:{HotelCode:hotelCode,Channel:Channel,RateCodeGroup:'3'},
            success:function(data){
                if(data.success === true)
                {
                    var DateFrom = '';
                    if (data['data']['firstAvailDate'] != null) {
                        DateFrom = data['data']['firstAvailDate'];
                        $('input[name="mobile_arrival_date"]').val(fn_formatDate(moment(DateFrom).format('YYYY-MM-DD')));
                        $('input[name="mobile_departure_date"]').val(fn_formatDate(moment(DateFrom).hours(24).minutes(0).seconds(0).format('YYYY-MM-DD')));
                    }
                    showMonthCalendar();
                    $.ajax({
                        type:'get',
                        url:api_getUnCheckDate_address,
                        dataType:'json',
                        data:{HotelCode:hotelCode,DateTo:DateTo,Channel:Channel,RateCodeGroup:'3'},
                        success:function(data){
                            if(data.success === true)
                            {
                                disabledDate = data['data'];

                                if (DateFrom != '') {
                                    hideMonthCalendar(DateFrom);
                                }
                            }
                        },
                        error:function(jqXHR){
                        }
                    });
                }
            },
            error:function(jqXHR){
            }
        });
    });
	//提交
$('#mobile_go_booking').click(function(){
    var http_host = $('input[name="http_host"]').val();
    //https://nonememberbe-qa.macausjm-glp.com/reservation/select-room?arrival_date=2020-12-24&departure_date=2020-12-25&rooms=1&adults=2&children=0&promotion_code=&hotel_code=GLPH&session_id=f58ac20c447a11eba5c3005056b3bb8e&group_by=room_type_cate&date=1608658125317
    var url = 'https://'+http_host+'/reservation/select-room?';
    url += 'arrival_date=' + changeDateType($('input[name="mobile_arrival_date"]').val());
    url += '&departure_date=' + changeDateType($('input[name="mobile_departure_date"]').val());
    url += '&rooms=' + $('#mobile_rooms').val();
    url += '&adults=' + $('#mobile_adults').val();
    url += '&children=' + $('#mobile_child').val();
    var promotion_code = $('#Mobile_PromotionCode').val();
    if (promotion_code != null && promotion_code != '' && promotion_code!=undefined) {
       url += '&promotion_code='+promotion_code; 
    }
    
    url += '&hotel_code=' + $('#mobile_booking_hotel').attr('data-value');;
    url += '&group_by=room_type_cate';
    url += '&lang=' + changeLanguage($("#current_language").val());
    url += '&skin=pvm';
    // alert(changeLanguage($("#current_language").val()));
    window.open(url, '_blank');
    // window.location.href = url;
});
var changeLanguage = function(language_code){
    if (language_code == 'zh-hant') {
        return 'tc';
    }else if(language_code == 'zh-hans'){
        return 'cn';
    }else{
        return 'en';
    }
}
				$('#mobile_booking').on('click',function () {
					$('.mask_calendar').show();
					// alert(disabledDate);
					renderCalendar();
					$('input[name="booking_type"]').val('booking');
				});
				$('.mask_calendar').on('click',function (e) {
					if(e.target.className == "mask_calendar"){
						$('.new-mobile-calendar').slideUp(200);
						$('.mask_calendar').fadeOut(200);
					} 
				})

				$('#mobile_sub_datepicker_from').on('click',function () {
					$('.mask_calendar').show();
					$(".new-mobile-calendar").slideToggle();
					renderCalendar();
					$('input[name="booking_type"]').val('room');
				});
				$('#mobile_sub_datepicker_to').on('click',function () {
					$('.mask_calendar').show();
					$(".new-mobile-calendar").slideToggle();
					renderCalendar();
					$('input[name="booking_type"]').val('room');
				});

				$('#mobile_sub_datepicker_from_offer').on('click',function () {
					$('.mask_calendar').show();
					$(".new-mobile-calendar").slideToggle();
					renderCalendar();
					$('input[name="booking_type"]').val('offer');
				});
				$('#mobile_sub_datepicker_to_offer').on('click',function () {
					$('.mask_calendar').show();
					$(".new-mobile-calendar").slideToggle();
					renderCalendar();
					$('input[name="booking_type"]').val('offer');
				});
				$('#mobile_booking').calendarSwitch({
					selectors : {
						sections : ".new-mobile-calendar"
					},
					index : 12,      //展示的月份个数
					animateFunction : "slideToggle",        //动画效果
					controlDay:true,//知否控制在daysnumber天之内，这个数值的设置前提是总显示天数大于90天
					// daysnumber : "300",     //控制天数
					comeColor : "#2EB6A8",       //入住颜色
					outColor : "#2EB6A8",      //离店颜色
					comeoutColor : "#E0F4F2",        //入住和离店之间的颜色
					callback :function(){//回调函数
						$('.mask_calendar').fadeOut(200);
						// var startDate = $('#startDate').val();  //入住的天数
						// var endDate = $('#endDate').val();      //离店的天数
						// var NumDate = $('.NumDate').text();    //共多少晚
						// console.log(startDate);
						// console.log(endDate);
						// console.log(NumDate);
						//下面做ajax请求
						//show_loading();
						/*$.post("demo.php",{startDate:startDate, endDate:endDate, NumDate:NumDate},function(data){
							if(data.result==1){
								//成功
							} else {
								//失败
							}
						});*/
					}  ,
					comfireBtn:'.comfire'//确定按钮的class或者id
				});
				// var b=new Date();
				// var ye=b.getFullYear();
				// var mo=b.getMonth()+1;
				// mo = mo<10?"0"+mo:mo;
				// var da=b.getDate();
				// da = da<10?"0"+da:da;
				// // $('#startDate').val(fn_formatDate(ye+'-'+mo+'-'+da));
				// b=new Date(b.getTime()+24*3600*1000);
				// var ye=b.getFullYear();
				// var mo=b.getMonth()+1;
				// mo = mo<10?"0"+mo:mo;
				// var da=b.getDate();
				// da = da<10?"0"+da:da;
				// $('#endDate').val(fn_formatDate(ye+'-'+mo+'-'+da));
			

var fn_formatDate=function(str){
    
    var now = new Date(str);
    var year = now.getFullYear(); 
    var month = now.getMonth();
    var day = now.getDate(); 
    if($("#current_language").val() == 'en'){
        var enMonth=new Array('January','February','March','April','May','June','July','August','September','October','November','December');
        return day +' '+ enMonth[month] +' '+ year;
    }else{
        var enMonth=new Array('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月');
        return year+'年' + enMonth[month] + day+'日';
    }
    
};

var changeDateType = function(date){
    if ($("#current_language").val() != 'en') {
    // date = date.replace(/[^\d]/g,'-');
    date = date.match(/\d{4}.\d{1,2}.\d{1,2}/mg).toString();  
	date = date.replace(/[^0-9]/mg, '-');
	var srt = date.split('-'); 
	if (srt[1] < 10) {
		srt[1] = '0'+srt[1];
	}
	if (srt[2] < 10) {
		srt[2] = '0'+srt[2];
	}
	date = srt[0]+'-'+srt[1]+'-'+srt[2];
	 	
    }
    
    return moment(date).format('YYYY-MM-DD');

}


//选择房间 number
$('.spinnerRoomsMobile').spinner({value:1,min:1,max:9});
$('.spinnerAdultsMobile').spinner({value:2,min:1,max:3});
$('.spinnerChildMobile').spinner({value:0,min:0,max:2});
//except the number area, click anywhere could close the collapse
    // $('#collapseNumber div').each(function(){
    //     $(this).attr('collapseNumber','yes');
    // });
    // $('div').click(function(){
    //     if($("#collapseNumber").hasClass("show")){
    //         if($(this).attr('collapseNumber')==='yes')  {
    //             return false;
    //         }
    //         else{
    //             $('#collapseNumber').removeClass("show");
    //         }
    //     }
    // });
    $('#collapseNumber_data_mobile a').click(function(){
        if($('#mobile_adults').val() == 3){
            $('#mobile_child').val(0);
            /*$('#mobile_child').parent('.spinner').find(".decrease").attr("disabled",true).css("pointer-events","none");
            $('#mobile_child').parent('.spinner').find(".increase").attr("disabled",true).css("pointer-events","none");*/
            $('#mobile_child').parent('.spinner').find(".decrease").attr("disabled",true).css("pointer-events","none");
            $('#mobile_child').parent('.spinner').find("increase").attr("disabled",true).css("pointer-events","none");
            //console.log($('#Child').parent('.spinner').find(".decrease"));
        }
        else{
            if($('#mobile_child').val() <2)
            {
                // $('#mobile_child').parent('.spinner').find(".increase").removeAttr("disabled").css("pointer-events","auto");
                $('#mobile_child').parent('.spinner').find(".increase").removeAttr("disabled").css("pointer-events","auto");
                $('#mobile_child').parent('.spinner').find(".decrease").removeAttr("disabled").css("pointer-events","auto");
            }
            
        }

        var content = '';
        var current_language= $("#current_language").val();
        if(current_language == "zh-hans"){
            var room=" 间客房 / ";
            var adult=" 位成人, ";
            var child=" 位儿童 ";
        }else if(current_language =="zh-hant"){
            var room=" 客房 / ";
            var adult=" 成人, ";
            var child=" 小童 ";
        }else {
            var room=" Room / ";
            var adult=" Adult, ";
            var child=" Child ";
        }
        if($('#mobile_rooms').val() > 1 && current_language == "en")
        {
            content += $('#mobile_rooms').val()+' Rooms / ';
        }
        else{
            content += $('#mobile_rooms').val()+room;
        }
        
        if($('#mobile_adults').val() > 1  && current_language == "en")
        {
            content += $('#mobile_adults').val()+' Adults, ';
        }
        else{
            content += $('#mobile_adults').val()+adult;
        }
        
        if($('#mobile_child').val() > 1  && current_language == "en")
        {
            content += $('#mobile_child').val()+' Children ';
        }
        else{
            if($('#mobile_child').val() == 0   && current_language == "en"){
                content += $('#mobile_child').val()+' Child ';
            }else {
                content += $('#mobile_child').val()+child;
            }

        }

        //var content=$('#Rooms').val()+' Room / '+$('#Adults').val()+' Adults / '+$('#Child').val()+' Child';
        $('#mobile_content').text(content);
    });


    //选择房间 number
$('.spinnerRoomsMobileSub').spinner({value:1,min:1,max:9});
$('.spinnerAdultsMobileSub').spinner({value:2,min:1,max:3});
$('.spinnerChildMobileSub').spinner({value:0,min:0,max:2});
//except the number area, click anywhere could close the collapse
    // $('#collapseNumber div').each(function(){
    //     $(this).attr('collapseNumber','yes');
    // });
    // $('div').click(function(){
    //     if($("#collapseNumber").hasClass("show")){
    //         if($(this).attr('collapseNumber')==='yes')  {
    //             return false;
    //         }
    //         else{
    //             $('#collapseNumber').removeClass("show");
    //         }
    //     }
    // });
    $('#collapseNumber_data_mobile_sub a').click(function(){
        if($('#mobile_sub_adults').val() == 3){
            $('#mobile_sub_child').val(0);
            $('#mobile_sub_child').parent('.spinner').find(".decrease").attr("disabled",true).css("pointer-events","none"); 
            $('#mobile_sub_child').parent('.spinner').find(".increase").attr("disabled",true).css("pointer-events","none");
            //console.log($('#Child').parent('.spinner').find(".decrease"));
        }
        else{
            if($('#mobile_sub_child').val() <2)
            {
                $('#mobile_sub_child').parent('.spinner').find(".increase").removeAttr("disabled").css("pointer-events","auto");
                $('#mobile_sub_child').parent('.spinner').find(".decrease").removeAttr("disabled").css("pointer-events","auto");
            }
            
        }

        var content = '';
        var current_language= $("#current_language").val();
        if(current_language == "zh-hans"){
            var room=" 间客房 / ";
            var adult=" 位成人, ";
            var child=" 位儿童 ";
        }else if(current_language =="zh-hant"){
            var room=" 客房 / ";
            var adult=" 成人, ";
            var child=" 小童 ";
        }else {
            var room=" Room / ";
            var adult=" Adult, ";
            var child=" Child ";
        }
        if($('#mobile_sub_rooms').val() > 1 && current_language == "en")
        {
            content += $('#mobile_sub_rooms').val()+' Rooms / ';
        }
        else{
            content += $('#mobile_sub_rooms').val()+room;
        }
        
        if($('#mobile_sub_adults').val() > 1  && current_language == "en")
        {
            content += $('#mobile_sub_adults').val()+' Adults, ';
        }
        else{
            content += $('#mobile_sub_adults').val()+adult;
        }
        
        if($('#mobile_sub_child').val() > 1  && current_language == "en")
        {
            content += $('#mobile_sub_child').val()+' Children ';
        }
        else{
            if($('#mobile_sub_child').val() == 0   && current_language == "en"){
                content += $('#mobile_sub_child').val()+' Child ';
            }else {
                content += $('#mobile_sub_child').val()+child;
            }

        }

        //var content=$('#Rooms').val()+' Room / '+$('#Adults').val()+' Adults / '+$('#Child').val()+' Child';
        $('#mobile_sub_content').text(content);
    });


$('.mobile_booking_view #hotellistul li').click(function(){
        var hotelCode = $('#mobile_booking_hotel').attr('data-value');
        var DateTo = moment().add(1, 'year').format('YYYY-MM-DD');
        var Channel = 'PAP';
            $.ajax({
        type:'get',
        url:api_getFirstAvailDate_address,
        dataType:'json',
        data:{HotelCode:hotelCode,Channel:Channel,RateCodeGroup:'3'},
        success:function(data){
            if(data.success === true)
            {
            	var DateFrom = '';
                if (data['data']['firstAvailDate'] != null) {
                DateFrom = data['data']['firstAvailDate'];
                $('input[name="mobile_arrival_date"]').val(fn_formatDate(moment(DateFrom).format('YYYY-MM-DD')));
                $('input[name="mobile_departure_date"]').val(fn_formatDate(moment(DateFrom).hours(24).minutes(0).seconds(0).format('YYYY-MM-DD')));
                }
                showMonthCalendar();
                $.ajax({
		        type:'get',
		        url:api_getUnCheckDate_address,
		        dataType:'json',
		        data:{HotelCode:hotelCode,DateTo:DateTo,Channel:Channel,RateCodeGroup:'3'},
		        success:function(data){
		            if(data.success === true)
		            {
		                disabledDate = data['data'];
		               
		                if (DateFrom != '') {
		                	hideMonthCalendar(DateFrom);
		                }
		            }
		        },
		    	error:function(jqXHR){
		        	}
		    	});
            }
        },
    	error:function(jqXHR){
        	}
    	});
    });

	
	$('#mobile_sub_booking_room').click(function(){

		// var hotelCode = 'GLPH';
		var hotelCode = $('input[name="hotelname_room"]').attr('data-value');
	    var DateTo = moment().add(1, 'year').format("YYYY-MM-DD");
	    var Channel = 'PAP';
	    var RoomCode = $('input[name="roomtype"]').val();
	    $.ajax({
	        //请求方式
	        type:'get',
	        //发送请求的地址
	        url:api_getFirstAvailDate_address,
	        //服务器返回的数据类型
	        dataType:'json',
	        data:{HotelCode:hotelCode,Channel:Channel,RoomCode:RoomCode,RateCodeGroup:'3'},
	        success:function(data){
	            //请求成功函数内容
	            //data = ajax_data;
	            if(data.success === true)
	            {
	                var DateFrom = '';
	                if (data['data']['firstAvailDate'] != null) {
	                DateFrom = data['data']['firstAvailDate'];
	                $('input[name="mobile_sub_arrival_date"]').val(fn_formatDate(moment(DateFrom).format('YYYY-MM-DD')));
	                $('input[name="mobile_sub_departure_date"]').val(fn_formatDate(moment(DateFrom).hours(24).minutes(0).seconds(0).format('YYYY-MM-DD')));
	                }
	                showMonthCalendar();

	                 $.ajax({
				        //请求方式
				        type:'get',
				        //发送请求的地址
				        url:api_getUnCheckDate_address,
				        //服务器返回的数据类型
				        dataType:'json',
				        data:{HotelCode:hotelCode,DateTo:DateTo,Channel:Channel,RoomCode:RoomCode,RateCodeGroup:'3'},
				        success:function(data){
				            //请求成功函数内容
				            //data = ajax_data;
				            if(data.success === true)
				            {
				                disabledDate = data['data'];
				                if (DateFrom != '') {
				                	hideMonthCalendar(DateFrom);
				                }
				            }
				        },
				    error:function(jqXHR){
				            //请求失败函数内容
				        }
				    });
	            }
	        },
		    error:function(jqXHR){
		            //请求失败函数内容
		        }
	    });
	});

	$('#mobile_go_booking_room').click(function(){
    var http_host = $('input[name="http_host"]').val();
    //https://nonememberbe-qa.macausjm-glp.com/reservation/select-room?arrival_date=2020-12-24&departure_date=2020-12-25&rooms=1&adults=2&children=0&promotion_code=&hotel_code=GLPH&session_id=f58ac20c447a11eba5c3005056b3bb8e&group_by=room_type_cate&date=1608658125317
    var url = 'https://'+http_host+'/reservation/select-room?';
    url += 'arrival_date=' + changeDateType($('input[name="mobile_sub_arrival_date"]').val());
    url += '&departure_date=' + changeDateType($('input[name="mobile_sub_departure_date"]').val());
    url += '&rooms=' + $('#Rooms1').val();
    url += '&adults=' + $('#Adults1').val();
    url += '&children=' + $('#Child1').val();

    url += '&hotel_code=' + $('input[name="hotelname_room"]').attr('data-value');
    url += '&group_by=room_type_cate';
    url += '&roomgroup=' + $('input[name="roomgroup"]').val();
    url += '&lang=' + changeLanguage($("#current_language").val());
    url += '&skin=pvm';
    window.open(url, '_blank');
    // window.location.href = url;
	});


	$('#mobile_sub_booking_offer').click(function(){
		// var hotelCode = 'GLPH';
		var hotelCode = $('input[name="hotelname_offer"]').attr('data-value');
    var DateTo = moment().add(1, 'year').format("YYYY-MM-DD");
    var Channel = 'PAP';
    var RateCode = $('input[name="rate"]').val();
    $.ajax({
        //请求方式
        type:'get',
        //发送请求的地址
        url:api_getFirstAvailDate_address,
        //服务器返回的数据类型
        dataType:'json',
        data:{HotelCode:hotelCode,DateTo:DateTo,Channel:Channel,RateCode:RateCode},
        success:function(data){
            //请求成功函数内容
            //data = ajax_data;
            if(data.success === true)
            {
                var DateFrom = '';
                if (data['data']['firstAvailDate'] != null) {
                DateFrom = data['data']['firstAvailDate'];
                $('input[name="mobile_sub_arrival_date_offer"]').val(fn_formatDate(moment(DateFrom).format('YYYY-MM-DD')));
                $('input[name="mobile_sub_departure_date_offer"]').val(fn_formatDate(moment(DateFrom).hours(24).minutes(0).seconds(0).format('YYYY-MM-DD')));
                }
                showMonthCalendar();
                $.ajax({
			        //请求方式
			        type:'get',
			        //发送请求的地址
			        url:api_getUnCheckDate_address,
			        //服务器返回的数据类型
			        dataType:'json',
			        data:{HotelCode:hotelCode,DateTo:DateTo,Channel:Channel,RateCode:RateCode},
			        success:function(data){
			            //请求成功函数内容
			            //data = ajax_data;
			            if(data.success === true)
			            {
			                disabledDate = data['data'];
			                if (DateFrom != '') {
				                	hideMonthCalendar(DateFrom);
				                }
			            }
			        },
			    	error:function(jqXHR){
			            //请求失败函数内容
			        }
		    	});
            }
        },
    error:function(jqXHR){
            //请求失败函数内容
        }
    	});
	});
	$('#mobile_go_booking_offer').click(function(){
    var http_host = $('input[name="http_host"]').val();
    //https://nonememberbe-qa.macausjm-glp.com/reservation/select-room?arrival_date=2020-12-24&departure_date=2020-12-25&rooms=1&adults=2&children=0&promotion_code=&hotel_code=GLPH&session_id=f58ac20c447a11eba5c3005056b3bb8e&group_by=room_type_cate&date=1608658125317
    var url = 'https://'+http_host+'/reservation/select-room?';
    url += 'arrival_date=' + changeDateType($('input[name="mobile_sub_arrival_date_offer"]').val());
    url += '&departure_date=' + changeDateType($('input[name="mobile_sub_departure_date_offer"]').val());
    url += '&rooms=' + $('#Rooms1').val();
    url += '&adults=' + $('#Adults1').val();
    url += '&children=' + $('#Child1').val();

    url += '&hotel_code='+$('input[name="hotelname_offer"]').attr('data-value');
    url += '&group_by=rate_code';
    url += '&rate=' + $('input[name="rate"]').val();
    url += '&lang=' + changeLanguage($("#current_language").val());
    url += '&skin=pvm';
    window.open(url, '_blank');
    // window.location.href = url;
	});

	function  renderCalendar(){
		var date_str = '';
		var arry = [];
		var strDays = new Date().getDate();
		$('.mobile_book_td').removeClass('calendar_selected');
		$('.mobile_book_td').removeClass('calendar_in_range');
		$('.mobile_book_td').each(function(index, element){
						if ($(this).text() != '') {
							date_str = moment($(this).attr('data-info')).format('YYYY-MM-DD');
							if ($.inArray(date_str, disabledDate) > -1) {
								if (!$(this).hasClass("calendar_disabled")) {
									$(this).addClass('calendar_disabled');
								}	
							}else{
								$(this).removeClass('calendar_disabled');
							}
							arry.push(element);
						}
                	})
                	for (var i = 0; i < strDays - 1; i++) {
                 	$(arry[i]).addClass('calendar_disabled');
               		}

	}

	function hideMonthCalendar(DateFrom){
		var dateNow = new Date();
		var yearNow = dateNow.getFullYear(); 
		var monNow = dateNow.getMonth() + 1;
		var dateBooking = new Date(DateFrom);
		var yearBooking = dateBooking.getFullYear(); 
		var monBooking = dateBooking.getMonth() + 1;
		for (var i = 0; i < 12; i++) {
			if (yearNow == yearBooking && monNow == monBooking) {
				break;
			}else{
				$('.'+yearNow+'-'+monNow).css('display','none');
				$('.'+yearNow+'-'+monNow).next().css('display','none');
			}
			if (monNow == 12) {
				yearNow ++;
				monNow = 1;
			}else{
				monNow ++;
			}
		}
	}
	function showMonthCalendar(){
		
		$('.mask_calendar .tbody p').css('display','revert');
        $('.mask_calendar .tbody table').css('display','revert');
	}





});

})(jQuery);