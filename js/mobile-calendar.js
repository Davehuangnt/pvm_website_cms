(function($) {
    var calendarSwitch = (function() {
        function calendarSwitch(element, options) {
            this.settings = $.extend(true, $.fn.calendarSwitch.defaults, options || {});
            this.element = element;
            this.startDate;
            this.endDate;
            this.arryDate = [];
            this.booking_type = '';
            this.month_list=new Array('January','February','March','April','May','June','July','August','September','October','November','December');
            this.init();
        }
        calendarSwitch.prototype = { /*说明：初始化插件*/
            /*实现：初始化dom结构，布局，分页及绑定事件*/
            init: function() {
                var me = this;
                me.selectors = me.settings.selectors;
                me.sections = me.selectors.sections;
                me.index = me.settings.index;
                me.comfire = me.settings.comfireBtn;
                var language  = $("#current_language").val();
                var theme_path  = $("#theme_path").val();
                if (language == 'en') {
                    var html = "<div class='headerWrapper'><div class='headerWrapper_div'><img style='height:60px;' src='https://qa.palazzoversace.mo/themes/pvm_website_cms/images/logo.png'></div><div class='comfire'><img src='"+theme_path+"/images/close.png'></div></div><table class='dateZone'><tr><td class='colo'>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td class='colo'>Sat</td></tr></table>" + "<div class='tbody'></div>";
                }else{
                    var html = "<div class='headerWrapper'><div class='headerWrapper_div'><img style='height:60px;' src='https://qa.palazzoversace.mo/themes/pvm_website_cms/images/logo.png'></div><div class='comfire'><img src='"+theme_path+"/images/close.png'></div></div><table class='dateZone'><tr><td class='colo'>日</td><td>一</td><td>二</td><td>三</td><td>四</td><td>五</td><td class='colo'>六</td></tr></table>" + "<div class='tbody'></div>"
                }
                
                $(me.sections).append(html);
                $(me.sections).find('.headerWrapper').css({
                    //"height": "71px",
                    "line-height": "71px",
                    "width":"100%",
                    "position": "fixed",
                    "z-index":"9999",
                    "text-align":"center"
                });
                $(me.sections).find('.headerTip').css({
                    "text-align": "left",
                    "line-height": "71px",
                    "margin-left":"10px",
                    "font-size":"15px"
                });
                $(me.sections).find(me.comfire).css({
                    "height": "30px",
                    "width": "16px",
                    "color": "#fff",
                    "right": "25px",
                    "text-align": "center",
                    "font-size": "0",
                    "cursor": "pointer",
                    "top": "27px",
                    "zoom": "normal",
                    "border": "none",
                    "text-shadow": "none",
                    "opacity": "1",
                    "font-weight": "700",
                    "float": "right",
                    "width": "100%",
                    //"background": "#fff",
                    "line-height": "30px",
                    "text-align": "right",
                    "padding-right": "15px",
                    //"box-shadow": "0 5px 10px -5px grey",
                    "margin": "8px 0 0 0",
                });
                if (language == 'en') {
                    var weekhtml = "<table class='New_dateweek'><tr><td class='colo'>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td class='colo'>Sat</td></tr></table>";
                }else{
                    var weekhtml = "<table class='New_dateweek'><tr><td class='colo'>日</td><td>一</td><td>二</td><td>三</td><td>四</td><td>五</td><td class='colo'>六</td></tr></table>";
                }
                for (var q = 0; q < me.index; q++) {
                    var select = q;
                    $(me.sections).find(".tbody").append("<p class='ny1'></p>"+weekhtml+"<table class='dateTable'></table>")
                    var start_at =  $('input[name="moblie_calendar_date"]').val();
                    if (start_at != '') {
                        var currentDate = new Date(start_at);
                    }else{
                        var currentDate = new Date();
                    }
                    
                    var currentMonth = currentDate.getMonth() + select;
                    var currentYear = currentDate.getFullYear();
                    if (currentMonth > 11) {
                        currentMonth = currentMonth - 12;
                        currentYear++;
                    }
                    // currentDate.setMonth(currentDate.getMonth() + select);
                    
                    // var currentMonth = currentDate.getMonth();
                    var setcurrentDate = new Date(currentYear, currentMonth, 1);
                    var firstDay = setcurrentDate.getDay();
                    var yf = currentMonth + 1;
                    if (language == 'en') {
                         $(me.sections).find('.ny1').eq(select).text( me.month_list[yf-1] + ' '+ currentYear);
                         $(me.sections).find('.ny1').eq(select).addClass( currentYear + '-'+ yf);
                    }else{
                        // if (yf < 10) {
                            // $(me.sections).find('.ny1').eq(select).text(currentYear + '年' + '0' + yf + '月');
                        // } else {
                            $(me.sections).find('.ny1').eq(select).text(currentYear + '年' + yf + '月');
                            $(me.sections).find('.ny1').eq(select).addClass( currentYear + '-'+ yf);
                        // }
                    }
                    
                    var DaysInMonth = [];
                    if (me._isLeapYear(currentYear)) {
                        DaysInMonth = new Array(31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
                    } else {
                        DaysInMonth = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
                    }
                    var Ntd = firstDay + DaysInMonth[currentMonth];
                    var Ntr = Math.ceil(Ntd / 7);
                    for (var i = 0; i < Ntr; i++) {
                        $(me.sections).find('.dateTable').eq(select).append('<tr></tr>');
                    };
                    var createTd = $(me.sections).find('.dateTable').eq(select).find('tr');
                    var td_first = 0;
                    createTd.each(function(index, element) {
                        for (var j = 0; j < 7; j++) {
                            if (firstDay == j && td_first == 0) {
                                td_first  = 1;
                            }
                            if (td_first > 0) {
                                var td_html = '<td class="mobile_book_td" data-info="'+ currentYear + '-';
                                if (yf < 10) {
                                    td_html = td_html + '0' +yf;
                                }else{
                                    td_html = td_html +yf;
                                }
                                if (td_first < 10) {
                                    td_html = td_html + '-0' +td_first+'"></td>';
                                }else{
                                    td_html = td_html + '-'+td_first+'"></td>';
                                }
                                td_first++;
                            }else{
                                var td_html = '<td class="mobile_book_td" ></td>';
                            }
                            $(this).append(td_html);

                        }
                    });
                    var arryTd = $(me.sections).find('.dateTable').eq(select).find('td');
                    for (var m = 0; m < DaysInMonth[currentMonth]; m++) {
                        arryTd.eq(firstDay++).text(m + 1);
                    }
                }
                me._initselected();

                me.element.on('click', function(event) {
                    event.preventDefault();
                    me._slider(me.sections)
                });
                //点击完成部分
                $(me.comfire).on('click', function(event) {
                    event.preventDefault();
                    me._slider(me.sections);
                    me._callback();
                    $(me.sections).find('.tbody .rz').each(function(index, element) {
                        //判断是否有CheckOut显示，没有就不给完成提交
                        if ($(this).text() == 'CheckOut') {
                            //点击的日期存入input
                            $(me.sections).find('.tbody .rz').each(function(index, element) {
                                if ($(this).text() == 'CheckIn') {
                                    var day = parseInt($(this).parent().text().replace(/[^0-9]/ig, "")) //截取字符串中的数字
                                    if(day < 10){
                                        day = "0" + day;
                                    }
                                    var startDayArrays = $(this).parents('table').prev('p').text().split('');
                                    var startDayArrayYear = [];
                                    var startDayArrayMonth = [];
                                    var startDayYear = "";
                                    var startDayMonth = "";
                                    for (var i = 0; i < me.index; i++) {
                                        var select = i;
                                        startDayArrayYear.push(startDayArrays[select])
                                    }
                                    startDayYear = startDayArrayYear.join('');
                                    for (var i = 5; i < 7; i++) {
                                        startDayArrayMonth.push(startDayArrays[i])
                                    }
                                    startDayMonth = startDayArrayMonth.join('');
                                    //添加入住到input
                                    //$('#startDate').val(startDayYear + '-' + startDayMonth + '-' + day);

                                    // $('#startDate').val(fn_formatDate(startDayYear + '-' + day));  //20210324 bug fix
                                }
                                if ($(this).text() == 'CheckOut') {
                                   // var day = parseInt($(this).parent().text().replace(/[^0-9]/ig, "").substring(0, 2));
                                    //var day = $(this).parent().text().split('离')[0];
                                    var day = $(this).parent().text().split('CheckOut')[0];//20210324 bug fix
                                    if(day < 10){
                                        day = "0" + day;
                                    }
                                    var endDayArrays = $(this).parents('table').prev('p').text().split('');
                                    var endDayArrayYear = [];
                                    var endDayArrayMonth = [];
                                    var endDayYear = "";
                                    var endDayMonth = "";
                                    for (var i = 0; i < 4; i++) {
                                        endDayArrayYear.push(endDayArrays[i])
                                    }
                                    endDayYear = endDayArrayYear.join('');
                                    for (var i = 5; i < 7; i++) {
                                        endDayArrayMonth.push(endDayArrays[i])
                                    }
                                    endDayMonth = endDayArrayMonth.join('');
                                    //添加入住到input
                                    // $('#endDate').val(fn_formatDate(endDayYear + '-' + endDayMonth + '-' + day));//20210324 bug fix
                                   // console.log($("#startDate").val().replace(/[^0-9]/ig, ""))
                                   // console.log($("#endDate").val().replace(/[^0-9]/ig, ""))
                                   // 如果入住等于CheckOut
                                   if (parseInt($("#startDate").val().replace(/[^0-9]/ig, "")) == parseInt($("#endDate").val().replace(/[^0-9]/ig, ""))) {
                                    var x = $('#startDate').val();
                                    var a = new Date(x.replace(/-/g, "/"));
                                    var b = new Date();
                                    b = new Date(a.getTime() + 24 * 3600 * 1000);
                                    var ye = b.getFullYear();
                                    var mo = b.getMonth() + 1;
                                    var da = b.getDate();
                                    $('#endDate').val(ye + '-' + mo + '-' + da);
                                }
                            }
                            startDayArrayYear = [];
                            startDayArrayMonth = [];
                            endDayArrayYear = [];
                            endDayArrayMonth = [];

                        });
                            //添加晚数
                            var NumDate = $('.lidian_hover').text().replace(/[^0-9]/ig,"");
                            $('.NumDate').text(NumDate);

                            var st = $('#startDate').val();
                            var en = $('#endDate').val();
                            //如果入住没值
                            if (st) {
                                me._slider(me.sections)
                                me._callback();
                            } else {
                                var b = new Date();
                                var ye = b.getFullYear();
                                var mo = b.getMonth() + 1;
                                var da = b.getDate();
                                $('#startDate').val(ye + '-' + mo + '-' + da);
                                b = new Date(b.getTime() + 24 * 3600 * 1000);
                                var ye = b.getFullYear();
                                var mo = b.getMonth() + 1;
                                var da = b.getDate();
                                $('#endDate').val(ye + '-' + mo + '-' + da);
                                $('.NumDate').text("1");
                                // alert("请选择入住CheckOut日期")
                                me._slider(me.sections)
                                me._callback()
                            }
                        }
                    });
});

},
_isLeapYear: function(year) {
    return (year % 4 == 0) && (year % 100 != 0 || year % 400 == 0);
},
_slider: function(id) {
    var me = this;
    me.animateFunction = me.settings.animateFunction;
    if (me.animateFunction == "fadeToggle") {
        $(id).fadeToggle();
    } else if (me.animateFunction == "slideToggle") {
        $(id).slideToggle();
    } else if (me.animateFunction == "toggle") {
        $(id).toggle();
    }
},
_initselected: function() {
    var me = this;
    me.comeColor = me.settings.comeColor;
    me.outColor = me.settings.outColor;
    me.daysnumber = me.settings.daysnumber;
    var strDays = new Date().getDate();
    var arry = [];
    var arry1 = [];
                // var tds = $(me.sections).find('.dateTable').eq(0).find('td');
                // tds.each(function(index, element) {
                //     if ($(this).text() == strDays) {
                //         var r = index;
                //         // $(this).append('</br><p class="rz">CheckIn</p>');
                //         if ($(this).next().text() != "") {
                //             // $(this).next().append('</br><p class="rz">CheckOut</p>');
                //         } else {
                //             $(".dateTable").eq(1).find("td").each(function(index, el) {
                //                 if ($(this).text() != "") {
                //                     // $(this).append('</br><p class="rz">CheckOut</p>');
                //                     return false;
                //                 }
                //             });
                //         }
                //         me._checkColor(me.comeColor, me.outColor)

                //     }
                // })

                $(me.sections).find('.tbody').find('td').each(function(index, element) {
                    if ($(this).text() != '') {
                        arry.push(element);
                    }
                });
                // for (var i = 0; i < strDays - 1; i++) {
                //     $(arry[i]).addClass('calendar_disabled');
                // }
                // if (me.daysnumber) {
                //     //可以在这里添加90天的条件
                //     for (var i = strDays - 1; i < strDays + parseInt(me.daysnumber); i++) {
                //         arry1.push(arry[i])
                //     }
                //     for (var i = strDays + parseInt(me.daysnumber); i < $(arry).length; i++) {
                //         $(arry[i]).addClass('calendar_disabled');
                //     }
                // } else {
                //     for (var i = strDays - 1; i < $(arry).length; i++) {
                //         arry1.push(arry[i])
                //     }
                // }
                if (me.daysnumber) {
                    //可以在这里添加90天的条件
                    for (var i = 0; i < strDays + parseInt(me.daysnumber); i++) {
                        arry1.push(arry[i])
                    }
                    for (var i = parseInt(me.daysnumber); i < $(arry).length; i++) {
                        $(arry[i]).addClass('calendar_disabled');
                    }
                } else {
                    for (var i = 0; i < $(arry).length; i++) {
                        arry1.push(arry[i])
                    }
                }
                me._selectDate(arry1)
            },
            _checkColor: function(comeColor, outColor) {
                var me = this;
                var rz = $(me.sections).find('.rz');
                // console.log(rz);
                for (var i = 0; i < rz.length; i++) {
                    if (rz.eq(i).text() == "CheckIn") {
                        rz.eq(i).closest('td').css({
                            'background': '#af9772',
                            'color': '#fff'
                        });
                    } else {
                        rz.eq(i).closest('td').css({
                            'background': '#af9772',
                            'color': '#fff'
                        });
                    }
                }

            },
            _callback: function() {
                var me = this;
                if (me.settings.callback && $.type(me.settings.callback) === "function") {
                    me.settings.callback();
                }
            },
            _clickCheckInDate: function(){
                // if (e(t.target).hasClass("available") && !(e(t.target).hasClass("active") && e(t.target).hasClass("start-date") && e(t.target).hasClass("end-date"))) {
                    // var start =
                    var api_host = $('input[name="api_host"]').val();
                    var api_getUnCheckDate_address = api_host+'/Availability/GetUnCheckDate';
                    var CheckInDate = moment(this.startDate).format("YYYY-MM-DD");
                    var DateTo = moment(CheckInDate).add(7, 'day').format("YYYY-MM-DD");
                    var Channel = 'PAP';
                    var lastday = '';
                    var me = this;
                    var booking_type = $('input[name="booking_type"]').val();
                    if(booking_type == 'booking') {
                        var hotelCode = $('#mobile_booking_hotel').attr('data-value');
                        $.ajax({
                            type:'get',
                            url:api_getUnCheckDate_address,
                            dataType:'json',
                            data:{HotelCode:hotelCode,DateTo:DateTo,Channel:Channel,CheckInDate:CheckInDate,RateCodeGroup:'3'},
                            success:function(data){
                                if(data.success === true)
                                {
                                    if (data['data'].length > 0) {
                                        $('.mobile_book_td').addClass('calendar_disabled');
                                        // DateTo = moment(data['data'][0]).add(-1,"days").format("YYYY-MM-DD");
                                        DateTo = data['data'][0];
                                    }else{
                                        DateTo = moment(CheckInDate).add(8, 'day').format("YYYY-MM-DD");
                                    }
                                    me._renderCalendar(me._getAllDate(CheckInDate,DateTo));
                                    
                                    // calendarObj.minDate = calendarObj.startDate
                                    // calendarObj.maxDate = moment(DateTo).add(-1,"days");
                                    // calendarObj.updateView();
                                }

                            },
                            error:function(jqXHR){
                            }
                        });  
                    }else if(booking_type == 'room'){
                        var hotelCode = $('input[name="hotelname_room"]').attr('data-value');
                        var RoomCode = jQuery('input[name="roomtype"]').val();

                        $.ajax({
                            type:'get',
                            url:api_getUnCheckDate_address,
                            dataType:'json',
                            data:{HotelCode:hotelCode,DateTo:DateTo,Channel:Channel,CheckInDate:CheckInDate,RoomCode:RoomCode},
                            success:function(data){
                                if(data.success === true)
                                {
                                    if (data['data'].length > 0) {
                                        $('.mobile_book_td').addClass('calendar_disabled');
                                        // DateTo = moment(data['data'][0]).add(-1,"days").format("YYYY-MM-DD");
                                        DateTo = data['data'][0];
                                    }else{
                                        DateTo = moment(CheckInDate).add(8, 'day').format("YYYY-MM-DD");
                                    }
                                    me._renderCalendar(me._getAllDate(CheckInDate,DateTo));
                                }

                            },
                            error:function(jqXHR){
                            }
                        });
                    }else if (booking_type == 'offer') {
                        // var hotelCode = 'GLPH';
                        var hotelCode = $('input[name="hotelname_offer"]').attr('data-value');
                        var RateCode = $('input[name="rate"]').val();
                        $.ajax({
                            type:'get',
                            url:api_getUnCheckDate_address,
                            dataType:'json',
                            data:{HotelCode:hotelCode,DateTo:DateTo,Channel:Channel,CheckInDate:CheckInDate,RateCode:RateCode},
                            success:function(data){
                                if (data['data'].length > 0) {
                                        $('.mobile_book_td').addClass('calendar_disabled');
                                        // DateTo = moment(data['data'][0]).add(-1,"days").format("YYYY-MM-DD");
                                        DateTo = data['data'][0];
                                    }else{
                                        DateTo = moment(CheckInDate).add(8, 'day').format("YYYY-MM-DD");
                                    }
                                    me._renderCalendar(me._getAllDate(CheckInDate,DateTo));

                            },
                            error:function(jqXHR){
                            }
                        });
                    }




                // }
            },
            _getAllDate:function(begin, end){
                var ab = begin.split("-");
                    var ae = end.split("-");
                    var db = new Date();
                    db.setUTCFullYear(ab[0], ab[1] - 1, ab[2]);
                    var de = new Date();
                    de.setUTCFullYear(ae[0], ae[1] - 1, ae[2]);
                    var unixDb = db.getTime();
                    var unixDe = de.getTime();
                    var str = [];
                    for(var k = unixDb; k < unixDe;) {
                        str.push((new Date(parseInt(k))).format()) ;
                        // str += (new Date(parseInt(k))).format() + ",";
                        k = k + 24 * 60 * 60 * 1000;
                    }
                    return str;
            },
            _renderCalendar: function(arry){
                var date_str;
                $('.mobile_book_td').each(function(){
                    date_str = moment($(this).attr('data-info')).format('YYYY-MM-DD');
                    if ($.inArray(date_str, arry) > -1) {
                        $(this).removeClass('calendar_disabled');
                    }else{
                        $(this).addClass('calendar_disabled');
                    }
                });
            },
            _selectDate: function(arry1) {
                var me = this;
                me.comeColor = me.settings.comeColor;
                me.outColor = me.settings.outColor;
                me.comeoutColor = me.settings.comeoutColor;
                me.sections = me.selectors.sections;
                var flag = 0;
                var first;
                var sum;
                var second;
                $(arry1).on('click', function(index) {

                    index.stopPropagation();
                    //第一次点击
                    if (!$(this).hasClass('calendar_disabled')) {
                        if (flag == 0 || me.booking_type != $('input[name="booking_type"]').val()) {
                        // $(me.sections).find('.hover').remove();
                        // $(me.sections).find('.tbody').find('p').remove('.rz');
                        // $(me.sections).find('.tbody').find('br').remove();
                        // $(arry1).css({
                        //     'background': '#fff',
                        //     'color': '#333333'
                        // });
                        // $(this).append('<p class="rz">CheckIn</p>')
                        me.booking_type = $('input[name="booking_type"]').val();
                        first = $(arry1).index($(this));
                        me.startDate = $(this).attr('data-info');
                        // me._checkColor(me.comeColor, me.outColor)
                        $(this).addClass('calendar_selected');
                        flag = 1;
                        me._clickCheckInDate();
                        me.arryDate = [];
                        $('.mobile_book_td').each(function(index){
                            if ($(this).hasClass('calendar_disabled')) {
                                me.arryDate.push(1);
                            }else{
                                me.arryDate.push(0);
                            }
                            
                        });
                        //显示提示：选择CheckOut日期
                        // $(me.sections).find('.rz').each(function(index, element) {
                        //     if ($(this).text() == 'CheckIn') {
                        //         $(this).parent('td').append('<span class="hover ruzhu_hover">选择CheckOut日期</span>');
                        //         $(this).parent('td').css('position', 'relative');
                        //     }
                        // });
                        // $('.hover').css({
                        //     'position': 'absolute',
                        //     'left': '-17px',
                        //     'top': '0px'
                        // })
                        // $('.ruzhu_hover').css({
                        //     'width':'100%',
                        //     'height':'41px',
                        //     'left': '0px',
                        //     'top': '-45px',
                        //     'background':'#434949',
                        //     'color':'#fff',
                        //     'z-index':'9999'
                        // })
                    } else if (flag == 1) { //第二次点击
                        // $(me.sections).find('.rz').each(function(index, element) {
                        //     if ($(this).text() == 'CheckIn') {
                        //         $(this).parent('td').find('.ruzhu_hover').remove();
                        //         $(this).parent('td').css('position', 'relative');
                        //     }
                        // });
                        flag = 0;
                        second = $(arry1).index($(this));
                        //如果第一次点击比第二次大，则不显示
                        // if(first >= second){
                        //     $(me.sections).find('.hover').remove();
                        //     $(me.sections).find('.tbody').find('p').remove('.rz');
                        //     $(me.sections).find('.tbody').find('br').remove();
                        //     $(arry1).css({
                        //         'background': '#fff',
                        //         'color': '#333333'
                        //     });
                        //     $(this).append('<p class="rz">CheckIn</p>')
                        //     first = $(arry1).index($(this));
                        //     me._checkColor(me.comeColor, me.outColor)
                        //     flag = 1;
                        //     //显示提示：选择CheckOut日期
                        //     $(me.sections).find('.rz').each(function(index, element) {
                        //         if ($(this).text() == 'CheckIn') {
                        //             $(this).parent('td').append('<span class="hover ruzhu_hover">选择CheckOut日期</span>');
                        //             $(this).parent('td').css('position', 'relative');
                        //         }
                        //     });
                        //     $('.hover').css({
                        //         'position': 'absolute',
                        //         'left': '-17px',
                        //         'top': '0px'
                        //     })
                        //     $('.ruzhu_hover').css({
                        //         'width':'100%',
                        //         'height':'41px',
                        //         'left': '0px',
                        //         'top': '-45px',
                        //         'background':'#434949',
                        //         'color':'#fff',
                        //         'z-index':'9999'
                        //     });
                        //     return;
                        // }
                        // sum = Math.abs(second - first);
                        // if (sum == 0) {
                        //     sum = 1;
                        // }

                        if (first < second) {
                            // $(this).append('<p class="rz">CheckOut</p>')
                            first = first + 1;
                            for (first; first < second; first++) {
                                $(arry1[first]).addClass('calendar_in_range');
                            }
                            me.endDate = $(this).attr('data-info');
                            $(this).addClass('calendar_selected');
                            var booking_type = $('input[name="booking_type"]').val();
                            if (booking_type == 'booking') {
                                $('input[name="mobile_arrival_date"]').val(fn_formatDate(me.startDate));
                                $('input[name="mobile_departure_date"]').val(fn_formatDate(me.endDate));
                            }else if (booking_type == 'room') {
                                $('input[name="mobile_sub_arrival_date"]').val(fn_formatDate(me.startDate));
                                $('input[name="mobile_sub_departure_date"]').val(fn_formatDate(me.endDate));
                            }else if (booking_type == 'offer') {
                                $('input[name="mobile_sub_arrival_date_offer"]').val(fn_formatDate(me.startDate));
                                $('input[name="mobile_sub_departure_date_offer"]').val(fn_formatDate(me.endDate));
                            }
                            
                            me._slider(me.sections);
                            $('.mask_calendar').fadeOut(200);
                            // $('.mask_calendar').hide();
                        } else if (first == second) {
                            $('.mobile_book_td').each(function(index){
                                if (me.arryDate[index] == 1) {
                                    $(this).addClass('calendar_disabled');
                                }else{
                                    $(this).removeClass('calendar_disabled');
                                }
                            });
                            $('.mobile_book_td').removeClass('calendar_selected');

                            /*$(me.sections).find('.rz').text('CheckIn');
                            $(this).append('<p class="rz">CheckOut</p>');
                            $(this).find('.rz').css('font-size', '12px');
                            var e = $(this).text().replace(/[^0-9]/ig, "");
                            var c, d;
                            var a = new Array();
                            var b = new Array();
                            var f;
                            var same = $(this).parents('table').prev('p').text().replace(/[^0-9]/ig, "").split('');
                            for (var i = 0; i < 4; i++) {
                                a.push(same[i]);
                            }
                            c = a.join('');
                            for (var j = 4; j < 6; j++) {
                                b.push(same[j]);
                            }
                            d = b.join('');
                            f = c + '-' + d + '-' + e;
                            $("#startDate").val(f);*/

                        }
                        //  else if (first > second) {

                        //     $(me.sections).find('.rz').text('CheckOut');
                        //     $(this).append('<p class="rz">CheckIn</p>')
                        //     second = second + 1;
                        //     for (second; second < first; second++) {
                        //         $(arry1[second]).css({
                        //             'background': me.comeoutColor,
                        //             'color': '#333333'
                        //         });
                        //     }
                        // }
                        // $(me.sections).find('.rz').each(function(index, element) {
                        //     if ($(this).text() == 'CheckOut') {
                        //         $(this).parent('td').append('<span class="hover lidian_hover">共' + sum + '晚</span>');
                        //         $(this).parent('td').css('position', 'relative');
                        //     }

                        // });

                        // $('.hover').css({
                        //     'position': 'absolute',
                        //     'left': '-17px',
                        //     'top': '0px'
                        // })
                        // $('.ruzhu_hover').css({
                        //     'width':'200%',
                        //     'left': '0px',
                        //     'top': '-24px',
                        //     'background':'#434949',
                        //     'color':'#fff',
                        //     'z-index':'9999'
                        // })
                        // $('.lidian_hover').css({
                        //     'width':'100%',
                        //     'left': '0px',
                        //     'top': '-24px',
                        //     'background':'#434949',
                        //     'color':'#fff'
                        // })
                        // me._slider('firstSelect')


                        // var myweek = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];

                        // var st = new Date($('#startDate').val());
                        // var en = new Date($('#endDate').val());
                        // $('.week').text(myweek[st.getDay()])
                        // $('.week1').text(myweek[en.getDay()])
                        // me._checkColor(me.comeColor, me.outColor)



                    }
                }
                    //第二次点击结束
                    // console.log($('#startDate').val())
                    // console.log($('#endDate').val())
                })
}

}
return calendarSwitch;
})();
$.fn.calendarSwitch = function(options) {
    return this.each(function() {
        var me = $(this),
        instance = me.data("calendarSwitch");

        if (!instance) {
            me.data("calendarSwitch", (instance = new calendarSwitch(me, options)));
        }

        if ($.type(options) === "string") return instance[options]();
    });
};
$.fn.calendarSwitch.defaults = {
    selectors: {
        sections: "#calendar"
    },
    index: 4,
        //展示的月份个数
        animateFunction: "toggle",
        //动画效果
        controlDay: false,
        //知否控制在daysnumber天之内，这个数值的设置前提是总显示天数大于90天
        daysnumber: false,
        //控制天数
        comeColor: "#af9772",
        //入住颜色
        outColor: "red",
        //CheckOut颜色
        comeoutColor: "#af9772",
        //入住和CheckOut之间的颜色
        callback: "",
        //回调函数
        comfireBtn: '.comfire' //确定按钮的class或者id

    };

    Date.prototype.format = function() {
                    var s = '';
                    s += this.getFullYear() + '-'; // 获取年份。
                    if((this.getMonth() + 1) >= 10) {// 获取月份。
                        s += (this.getMonth() + 1) + "-";
                    } else {
                        s += "0" + (this.getMonth() + 1) + "-";
                    }
                    if(this.getDate() >= 10) {// 获取日。
                        s += this.getDate();
                    } else {
                        s += "0" + this.getDate();
                    }
                    return(s); // 返回日期。
                };


var fn_formatDate=function(str){
    
    var now = new Date(str);
    var year = now.getFullYear(); 
    var month = now.getMonth();
    var day = now.getDate(); 
    if($("#current_language").val() == 'en'){
        var enMonth=new Array('January','February','March','April','May','June','July','August','September','October','November','December');
        return day +' '+ enMonth[month] +' '+ year;
    }else{
        var enMonth=new Array('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月');
        return year+'年' + enMonth[month] + day+'日';
    }
    
};


})(jQuery);