(function($) {
    $(function () {
        var filterList = {

          init: function () {
            // MixItUp plugin
            $('#portfoliolist').mixitup({
              targetSelector: '.portfolio',
              filterSelector: '.filter',
              //effects: ['fade'],
              //easing: 'snap',
              transitionSpeed: 1,
              // call the hover effect
              onMixEnd: filterList.hoverEffect()
            });       
          },





          hoverEffect: function () {
            // Simple parallax effect
            $('#portfoliolist .portfolio').hover(
              function () {
                $(this).find('.label').stop().animate({bottom: 0}, 200, 'easeOutQuad');
              },
              function () {
                $(this).find('.label').stop().animate({bottom: -40}, 200, 'easeInQuad');
              }   
            );        
          }
        };
        // Run the show!
        filterList.init();
      }); 

      $(".Offers_tablist_dropdown .dropdown-menu ul li .filter").click(function(){
        $('#filter_content').html($(this).html());
    });


})(jQuery);

