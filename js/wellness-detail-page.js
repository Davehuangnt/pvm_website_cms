(function($) {
    var formatDateStr='YYYY-MM-DD';
  var fn_formatDate=function(str){
    var now = new Date(str);
    var year = now.getFullYear();
    var month = now.getMonth();
    var day = now.getDate();
    if($("#current_language").val() == 'zh-hant'){
      var enMonth=new Array('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月');
      return year+'年' + enMonth[month] + day+'日';
    }else if($("#current_language").val() == 'zh-hans'){
      var enMonth=new Array('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月');
      return year+'年' + enMonth[month] + day+'日';
    }else{
      var enMonth=new Array('January','February','March','April','May','June','July','August','September','October','November','December');
      return day +' '+ enMonth[month] +' '+ year;
    }

  };
  var minDate = new Date();
  $(function () {
      $(".media__body a").css("margin-top", ($(".media__image").height() - 50) / 2);
      if($("#current_language").val() == 'zh-hant'){
          var month_list=new Array('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月');
          var week_list=['日','一','二','三','四','五','六'];
      }else if($("#current_language").val() == 'zh-hans'){
          var week_list=['日','一','二','三','四','五','六'];
          var month_list=new Array('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月');
      }else{
          var week_list=['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
          var month_list=new Array('January','February','March','April','May','June','July','August','September','October','November','December');
      }
    var spa_date = [];
    if ($('.date_spas').length){
      $('.date_spas').each(function(){
        //spa_date.push( $(this).val());
        var dates= $(this).val();
        if(dates.indexOf("/") != -1){
          var datelist = dates.split(" / ");
          getdiffdate(datelist[0],datelist[1]);
        }else {
          // spa_date.push(dates);
          if ($.inArray(dates, spa_date) === -1) {
            spa_date.push(dates) ;
          }
        }
      });
      console.log(spa_date);
    }
    function getdiffdate(stime,etime){
      //初始化日期列表，数组
      var i=0;
      //开始日期小于等于结束日期,并循环
      while(stime<=etime){
        if ($.inArray(stime, spa_date) === -1) {
          spa_date.push(stime) ;
        }
        //spa_date.push(stime) ;

        //获取开始日期时间戳
        var stime_ts = new Date(stime).getTime();

        //增加一天时间戳后的日期
        var next_date = stime_ts + (24*60*60*1000);

        //拼接年月日，这里的月份会返回（0-11），所以要+1
        var next_dates_y = new Date(next_date).getFullYear()+'-';
        var next_dates_m = (new Date(next_date).getMonth()+1 < 10)?'0'+(new Date(next_date).getMonth()+1)+'-':(new Date(next_date).getMonth()+1)+'-';
        var next_dates_d = (new Date(next_date).getDate() < 10)?'0'+new Date(next_date).getDate():new Date(next_date).getDate();

        stime = next_dates_y+next_dates_m+next_dates_d;

      }
    }
    $('#MakeAReservationDate').val(fn_formatDate(minDate));
    var year = minDate.getFullYear();
    var month = minDate.getMonth() + 1 < 10 ? "0" + (minDate.getMonth() + 1)
        : minDate.getMonth() + 1;
    var day = minDate.getDate() < 10 ? "0" + minDate.getDate() : minDate
        .getDate();
    var dateStr = year + "-" + month + "-" + day;
    $('input[name="date"]').val(dateStr);
    $('#MakeAReservationDate').daterangepicker({
      minDate:moment().hours(0).minutes(0).seconds(0), //设置开始日期
      format : formatDateStr,
      singleDatePicker:true,
      autoUpdateInput:false,
      autoApply: true,
      SpecialClassname:'spaDatastep1',
      locale: {
        daysOfWeek: week_list,
        monthNames: month_list,
      },
      startDate: moment().add('days'),
      parentEl: "#BOOKModal",
      isInvalidDate: function(date) {
        if ($.inArray(date.format('YYYY-MM-DD'), spa_date) != -1) {
          return true;
        } else {
          return false;
        }

      },
    }, function(start) {
      //格式化日期显示框
      $('#MakeAReservationDate').val(fn_formatDate(start.format(formatDateStr)));
      $('input[name="date"]').val(start.format(formatDateStr));
    });

    $('#MakeAReservationDate').click(function(){
      $('.SPADatastep').addClass("displayblock");
    });
    $(window).scroll(function(){
      $('.SPADatastep').removeClass("displayblock");
    });
  })
  $("#BOOKModal_step3").hide();
  var screenW=document.body.clientWidth;
  if($('.swiper-container').length) {
    if (screenW > 992) {
      var loops=false;
      var number = $('.swiper-wrapper .swiper-slide').length;
      if(number > 3){
        loops=true;
        $(".swiper-button").show();
      }else {
        $(".swiper-button").hide();
      }
      var swiper = new Swiper('.swiper-container', {
        speed: 1000,
        slidesPerView: 3,
        spaceBetween: 30,
        slidesPerGroup: 1,
        loop: loops,
        autoplay: loops,
        loopFillGroupWithBlank: true,
        autoplayDisableOnInteraction: false,
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
      });
      $('.hide-box').hide();
    }
    if (screenW < 991) {
      var loops=false;
      var number = $('.swiper-wrapper .swiper-slide').length;
      if(number > 1){
        loops=true;
        $(".swiper-button").show();
      }else {
        $(".swiper-button").hide();
      }
      var swiper = new Swiper('.swiper-container', {
        speed: 1000,
        slidesPerView: 'auto',
        spaceBetween: 20,
        loop: loops,
        autoplay: loops,
        loopFillGroupWithBlank: true,
        autoplayDisableOnInteraction: false,
        on: {
          slideChangeTransitionEnd: function () {
            this.autoplay.start();
          },
        },
        pagination: {
          el: '.swiper-pagination',
          clickable: true,
        },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
      });
    }
  }
  $(".Therapist_select:nth-child(1)").click(function(){
    $('.Therapist_select:nth-child(1)').addClass("active");
    $("#gender_value").val($.trim($(this).text()));
    $(".Error_Tips_Therapist").hide();
    if($('.Therapist_select:nth-child(2)').hasClass("active") || $('.Therapist_select:nth-child(3)').hasClass("active") )
    {
      $('.Therapist_select:nth-child(2)').removeClass("active");
      $('.Therapist_select:nth-child(3)').removeClass("active");
    }
  });
  $(".Therapist_select:nth-child(2)").click(function(){
    $('.Therapist_select:nth-child(2)').addClass("active");
    $("#gender_value").val($.trim($(this).text()));
    $(".Error_Tips_Therapist").hide();
    if($('.Therapist_select:nth-child(1)').hasClass("active") || $('.Therapist_select:nth-child(3)').hasClass("active") )
    {
      $('.Therapist_select:nth-child(1)').removeClass("active");
      $('.Therapist_select:nth-child(3)').removeClass("active");
    }
  });
  $(".Therapist_select:nth-child(3)").click(function(){
    $('.Therapist_select:nth-child(3)').addClass("active");
    $("#gender_value").val($.trim($(this).text()));
    $(".Error_Tips_Therapist").hide();
    if($('.Therapist_select:nth-child(1)').hasClass("active") || $('.Therapist_select:nth-child(2)').hasClass("active") )
    {
      $('.Therapist_select:nth-child(1)').removeClass("active");
      $('.Therapist_select:nth-child(2)').removeClass("active");
    }
  });
  $(function() {
    $(".Captcha_refresh").on("click",function () {
      var pvm_path=$("#pvm_path").val();
      var url = pvm_path+"formajax/vcode?tmp="+Math.random();
      $("#code_image").attr('src',url);
    });
    var submit=true;
    $('#BOOKModal—step1-n').click(function(){
      var status=true;
      if($('#spaname').val()===''){
        $(".Error_Tips_spaname").show();
        status=false;
      }

      if($('#time').val()===''){
        $(".Error_Tips5").show();
        status=false;
      }
      if($('#gender_value').val()===''){
        $(".Error_Tips_Therapist").show();
        status=false;
      }
      if($('#spatype').val()===''){
        $(".Error_Tips_spatype").show();
        status=false;
      }
      if($('#Data_calander_input').val()===''){
        $(".Error_Tips5").show();
        status=false;
      }

      if(status){
        $('#spaform').hide();
        $('#spaform02').show();
      }

    });//第一步next
    $('#BOOKModal—step2-s').click(function(){
      var status=true;

      if($('#FirstName').val()===''){
        $(".Error_Tips1").show();
        status=false;
      }
      if(  $('#LastName').val()===''){
        $(".Error_Tips2").show();
        status=false;
      }
      if($('#FirstName').val()!=='' && $('#LastName').val()!==''){
        $(".Error_Tips1").hide();
        $(".Error_Tips2").hide();
      }
      if($('#TelephoneN').val()==='' ){
        $(".Error_Tips3").show();
        status=false;
      }
      var phone=$('#TelephoneN').val();
      if( (phone.length <7 || phone.length >15 ) && phone.length>0 ){
        $(".Error_Tips_Phone").show();
        $(".Error_Tips3").hide();
        status=false;
      }
      if($('#Email').val()===''){
        $(".Error_Tips4").show();
        status=false;
      }

      var $email_num = $("#Email").val();
      var reg01 = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/; //
      if (reg01.test($email_num)) {

      } else {
        $(".Error_Tips4").show();
        status=false;
      }
     
      if($('#titlesel').val()===''){
        $(".Error_Tips_Title").show();
        status=false;
      } 
      if($('#captcha').val()===''){
        $(".Error_Tips_Captcha").show();
        status=false;
      }
        if($("#check-me").is(':checked')){
            $(".Error_Tips6").hide();
        }else {
            $(".Error_Tips6").show();
            status=false;
        }
     if(status && submit){
       submit=false;
       var spaform = $('#spaform').serialize();


       var formdata = $('#spaform02').serialize();
       formdata= spaform +'&'+formdata +"&current_language="+ $("#current_language").val();
       var pvm_path=$("#pvm_path").val();
       $.ajax({
         url: pvm_path+"formajax/spaajax",
         type: "post",
         data: formdata,
         success: (response) => {
           var data=response[0].value;
           if(data.code === 1000){
             $("#BOOKModal_step1").hide();
             $('#spaform').show();
             $('#spaform02').hide();
             $("#BOOKModal_step2").show();
             $("#BOOKModal_step3").hide();
             var url = pvm_path+"formajax/vcode?tmp="+Math.random();
             if($("#current_language").val() == 'zh-hant'){
               $("#spatype_text").html('-請選擇療程類型-');
               $("#spaname_text").html('-請選擇療程-');
               $("#time_text").html('-請選擇-');
               $("#title_text").html('-請選擇-');
               $("#Treatmentname_text").html('-請選擇-');
               $(".Guests").val('1');
             }else if($("#current_language").val() == 'zh-hans'){
               $("#spatype_text").html('-请选择疗程类型-');
               $("#spaname_text").html('-请选择疗程-');
               $("#time_text").html('-请选择-');
               $("#title_text").html('-请选择-');
               $("#Treatmentname_text").html('-请选择-');
               $(".Guests").val('1');
             }else{
               $("#spatype_text").html('-Please select-');
               $("#spaname_text").html('-Please select-');
               $("#time_text").html('-Please Select-');
               $("#title_text").html('-Please Select-');
               $("#Treatmentname_text").html('-Please Select-');
               $(".Guests").val('1');
             }
             $("#spaname_ul").html('');
             $("#code_image").attr('src',url);
             $("#spatype").val('');
             $("#spaname").val('');
             $("#adult_value").val(1);
             $("#titlesel").val('');
             $("#FirstName").val('');
             $("#LastName").val('');
             $("#title").val('');
             $("#time").val('');
             $("#TelephoneN").val('');
             $("#gender_value").val('');
             $("#Email").val('');
             $("#captcha").val('');
             $("#Message").val('');
             $('#MakeAReservationDate').val(fn_formatDate(minDate));
             $(".Therapist_select").each(function () {
               $(this).removeClass("active");
             });
             $("#check-me").prop("checked",false);
           }else if(data.code === -1) {
             $(".Error_Tips_Captcha").show();
           }else {
             $("#BOOKModal_step1").hide();
             $('#spaform').show();
             $('#spaform02').hide();
             $("#BOOKModal_step2").hide();
             $("#BOOKModal_step3").show();
             var url = pvm_path+"formajax/vcode?tmp="+Math.random();
             if($("#current_language").val() == 'zh-hant'){
               $("#spatype_text").html('-請選擇療程類型-');
               $("#spaname_text").html('-請選擇療程-');
               $("#time_text").html('-請選擇-');
               $("#title_text").html('-請選擇-');
               $("#Treatmentname_text").html('-請選擇-');
               $(".Guests").val('1');
             }else if($("#current_language").val() == 'zh-hans'){
               $("#spatype_text").html('-请选择疗程类型-');
               $("#spaname_text").html('-请选择疗程-');
               $("#time_text").html('-请选择-');
               $("#title_text").html('-请选择-');
               $("#Treatmentname_text").html('-请选择-');
               $(".Guests").val('1');
             }else{
               $("#spatype_text").html('-Please select-');
               $("#spaname_text").html('-Please select-');
               $("#time_text").html('-Please Select-');
               $("#title_text").html('-Please Select-');
               $("#Treatmentname_text").html('-Please Select-');
               $(".Guests").val('1');
             }
             $("#spaname_ul").html('');
             $("#code_image").attr('src',url);
             $("#spatype").val('');
             $("#spaname").val('');
             $("#adult_value").val(1);
             $("#titlesel").val('');
             $("#FirstName").val('');
             $("#LastName").val('');
             $("#title").val('');
             $("#time").val('');
             $("#TelephoneN").val('');
             $("#gender_value").val('');
             $("#Email").val('');
             $("#captcha").val('');
             $("#Message").val('');
             $('#MakeAReservationDate').val(fn_formatDate(minDate));
             $(".Therapist_select").each(function () {
               $(this).removeClass("active");
             });
             $("#check-me").prop("checked",false);
           }
           setTimeout(function(){
             submit=true;
             status=true;
           },500);
         },
         error: function (data) {
           $("#BOOKModal_step1").hide();
           $('#spaform').show();
           $('#spaform02').hide();
           $("#BOOKModal_step2").hide();
           $("#BOOKModal_step3").show();
           var url = pvm_path+"formajax/vcode?tmp="+Math.random();
           $("#code_image").attr('src',url);
           setTimeout(function(){
             submit=true;
             status=true;
           },500);
         },
       });
     }
    });//第二步submit

    $('#BOOKModal—step2-b').click(function(){
      $('#spaform').show();
      $('#spaform02').hide();
    });//第二步back
    $('#BOOKModal—step2-o').click(function(){
      $('#BOOKModal').modal('hide');
      $("#spatype_text").removeClass('new_select_txt');
      $("#spaname_text").removeClass('new_select_txt');
      $("#time_text").removeClass('new_select_txt');
      $("#title_text").removeClass('new_select_txt');

      setTimeout(function () {
        $("#BOOKModal_step1").show();
        $('#spaform').show();
        $('#spaform02').hide();
        $("#BOOKModal_step2").hide();
        $("#BOOKModal_step3").hide();
      }, 500);

    });//第三步OK

    $("#check-me").focus(function(){
      $(".Error_Tips6").hide();
    })
    $("#FirstName").focus(function(){
      $(".Error_Tips1").hide();
    });
    $("#LastName").focus(function(){
      $(".Error_Tips2").hide();
    });
    $("#titlesel_ul").focus(function(){
      $(".Error_Tips_Title").hide();
    });
    $("#spaname_ul").focus(function(){
      $(".Error_Tips_spaname").hide();
    });
    $("#captcha").focus(function(){
      $(".Error_Tips_Captcha").hide();
    });
    $("#TelephoneN").focus(function(){
      $(".Error_Tips3").hide();
      $(".Error_Tips_Phone").hide();
    });
    $("#spatype_ul").focus(function(){
      $(".Error_Tips_spatype").hide();
    });
    $("#time_ul").focus(function(){
      $(".Error_Tips5").hide();
    });
    $("#Email").focus(function(){
      $(".Error_Tips4").hide();
    });
    $("#check-me").click(function(){
      $(".Error_Tips6").hide();
    });
 /*   $("#Data_calander_input").focus(function(){
      $(".Error_Tips5").hide();
    });*/
    $(".adult_jia").click(function(){
      var adult_value = $("#adult_value").val();
      var current_language= $("#current_language").val();
      if(current_language == "zh-hans"){
        var adult=" ";
      }else if(current_language =="zh-hant"){

        var adult=" ";
      }else {
        var adult=" ";
      }
      if(parseInt (adult_value) < 5)	{
        adult_value = parseInt (adult_value)+1;
        $(".Guests").val(adult_value+adult);
        $("#adult_value").val(adult_value);
      }

    });
    $(".adult_jian").click(function(){
      var adult_value = $("#adult_value").val();
      var current_language= $("#current_language").val();
      if(current_language == "zh-hans"){
        var adult=" ";
      }else if(current_language =="zh-hant"){

        var adult=" ";
      }else {
        if(parseInt (adult_value) >2){
          var adult=" ";
        }  else{
          var adult=" ";
        }
      }
      if(parseInt (adult_value) > 1)	{
        adult_value = parseInt (adult_value)-1;
        $(".Guests").val(adult_value+adult);
        $("#adult_value").val(adult_value);
      }
    });
    if ($('.spatype_info').length){
      var num=1;
      var html = "";
      var type=0;
      $('.spatype_info').each(function(){
        //spa_date.push( $(this).val());
        var spatype= $(this).val();
        if(spatype.indexOf("/") != -1){
          var spatypelist = spatype.split("/");
          if(num == 1){
            type=spatypelist[0];
          }
          // html +='<option value="'+spatypelist[1]+'" data-value="'+spatypelist[0]+'">'+spatypelist[1]+'</option>';
          html += ' <li data-value="'+spatypelist[0]+'">'+spatypelist[1]+'</li>';
          num++;
        }
      });
      $("#spatype_ul").html(html);
      $('#spatype_ul li').click(function(){
        $("#spatype_text").addClass('new_select_txt');
      });
   /*   $("select#spatype").change(function(){
        var parent=$('#spatype option:selected').attr("data-value");
        var current_language= $("#current_language").val();
        if(current_language == "zh-hans"){
          var firstsel='<option value="">-请选择-</option>';
        }else if(current_language =="zh-hant"){
          var firstsel='<option value="">-請選擇-</option>';
        }else {
          var firstsel='<option value="">-Please Select-</option>';
        }
        var html2=firstsel;
        if($('.spaname_info_'+parent).length){

          var num2=1;
          $('.spaname_info_'+parent).each(function(){
            var spaname= $(this).val();
            if(spaname.indexOf("/") != -1){
              var spanamelist = spaname.split("/");
              html2 += '<option value="'+spanamelist[1]+'" data-value="'+spanamelist[0]+'">'+spanamelist[1]+'</option>';
              num2++;
            }
          });
          $("#spaname").html(html2);
        }else {
          $("#spaname").html(html2);
        }
      });*/
   /*   if($('.spaname_info_'+type).length){
        var html2=""
        var num2=1;
        $('.spaname_info_'+type).each(function(){
          var spaname= $(this).val();
          if(spaname.indexOf("/") != -1){
            var spanamelist = spaname.split("/");
            // html2 += '<option value="'+spanamelist[1]+'" data-value="'+spanamelist[0]+'">'+spanamelist[1]+'</option>';
            html2 += ' <li data-value="'+spanamelist[0]+'">'+spanamelist[1]+'</li>';
            num2++;
          }
        });
        $("#spaname_ul").html(html2);
      }*/
    }
  });

  var screenW=document.body.clientWidth;
  if(screenW > 767){
    var bodyH=$(window).height();
    //$('#BOOKModal .modal-dialog').css('margin-top', (bodyH - 735 - 190) / 2);
      $('#BOOKModal .modal-dialog').css('margin-top', '125px');
    $(window).resize(function() {
      var bodyH=$(window).height();
      //$('#BOOKModal .modal-dialog').css('margin-top', (bodyH - 735) / 2);
      $('#BOOKModal .modal-dialog').css('margin-top', '125px');
    });
  }
  if(screenW < 767){
    $('.Telephone_row_select2 ul').css('width',document.body.clientWidth - 60);
  }


  //下拉
  $('.Telephone_row_area [name="nice-select"]').click(function(e){
    $(this).find('ul').toggle();
    e.stopPropagation();
    $('.Telephone_row_area').toggleClass('FormControl_dropdownHotel_icon');
  });
  $('.Telephone_row_area [name="nice-select"] li').hover(function(e){
    $(this).toggleClass('on');
    e.stopPropagation();
  });
  $('.Telephone_row_area [name="nice-select"] li').click(function(e){
    var val = $(this).text();
    $(this).parents('.Telephone_row_area [name="nice-select"]').find('input').val(val);
    $('.Telephone_row_area [name="nice-select"] ul').hide();
    e.stopPropagation();
    $('.Telephone_row_area').removeClass('FormControl_dropdownHotel_icon');
  });
  $(document).click(function(){
    $('.Telephone_row_area [name="nice-select"] ul').hide();
    $('.Telephone_row_area').removeClass('FormControl_dropdownHotel_icon');
  });
  $('#booNavigation').click(function(){
    $('.GLP_Header_wrap').removeClass('Header_Menu_scroll_Class');
  });
  $(".close").click(function () {

    $("#BOOKModal_step1").show();
    $("#BOOKModal_step2").hide();
    $('#spaform').show();
    $('#spaform02').hide();
    $("#BOOKModal_step3").hide();
    $(".Error_Tips6").hide();
    $(".Error_Tips0").hide();
    $(".Error_Tips1").hide();
    $(".Error_Tips2").hide();
    $(".Error_Tips_Title").hide();
    $(".Error_Tips_spaname").hide();
    $(".Error_Tips_Captcha").hide();
    $(".Error_Tips3").hide();
    $(".Error_Tips_Phone").hide();
    $(".Error_Tips_spatype").hide();
    $(".Error_Tips5").hide();
    $(".Error_Tips4").hide();
    $(".Error_Tips_Therapist").hide();
  });


  //下拉
  function diy_select(){this.init.apply(this,arguments)};
  diy_select.prototype={
     init:function(opt)
     {
       this.setOpts(opt);
       this.o=this.getByClass(this.opt.TTContainer,document,'div');//容器
       this.b=this.getByClass(this.opt.TTDiy_select_btn);//按钮
       this.t=this.getByClass(this.opt.TTDiy_select_txt);//显示
       this.l=this.getByClass(this.opt.TTDiv_select_list);//列表容器
       this.ipt=this.getByClass(this.opt.TTDiy_select_input);//列表容器
       this.lengths=this.o.length;
       this.showSelect();
     },
     addClass:function(o,s)//添加class
     {
       o.className = o.className ? o.className+' '+s:s;
     },
     removeClass:function(o,st)//删除class
     {
       var reg=new RegExp('\\b'+st+'\\b');
       o.className=o.className ? o.className.replace(reg,''):'';
     },
     addEvent:function(o,t,fn)//注册事件
     {
       return o.addEventListener ? o.addEventListener(t,fn,false):o.attachEvent('on'+t,fn);
     },
     showSelect:function()//显示下拉框列表
     {
       var This=this;
       var iNow=0;
       this.addEvent(document,'click',function(){
          for(var i=0;i<This.lengths;i++)
          {
            This.l[i].style.display='none';
          }
       })
       for(var i=0;i<this.lengths;i++)
       {
         this.l[i].index=this.b[i].index=this.t[i].index=i;
         this.t[i].onclick=this.b[i].onclick=function(ev)  
         {
           var e=window.event || ev;
           var index=this.index;
           This.item=This.l[index].getElementsByTagName('li');
           var id =This.ipt[index].id;
           if(id == "spaname"){
             if($('#spatype').val()===''){
               $(".Error_Tips_spatype").show();
               $(".diy_select_list").css('border','none');
             }
           }
           This.l[index].style.display= This.l[index].style.display=='block' ? 'none' :'block';
           for(var j=0;j<This.lengths;j++)
           {
             if(j!=index)
             {
               This.l[j].style.display='none';
             }
           }
           This.addClick(This.item);
           e.stopPropagation ? e.stopPropagation() : (e.cancelBubble=true); //阻止冒泡
         }
       }
     },
     addClick:function(o)//点击回调函数
     {

       if(o.length>0)
       {
         var This=this;
         for(var i=0;i<o.length;i++)
         {
           o[i].onmouseover=function()
           {
             This.addClass(this,This.opt.TTFcous);
           }
           o[i].onmouseout=function()
           {
             This.removeClass(this,This.opt.TTFcous);
           }
           o[i].onclick=function()
           {
             var index=this.parentNode.index;//获得列表
             //This.t[index].innerHTML=This.ipt[index].value=this.innerHTML.replace(/^\s+/,'').replace(/\s+&/,'');
             This.t[index].innerHTML=This.ipt[index].value=this.innerHTML;
             This.l[index].style.display='none';
             var id =This.ipt[index].id;
             var type=this.getAttribute("data-value");
             if(id == "spatype"){
               $(".Error_Tips_spatype").hide();
               $("#spaname").val('');
               $(".Reservation_Error_tips_spatype").hide();
               if($("#current_language").val() == 'zh-hant'){
                 $("#spaname_text").html('-請選擇療程-');
               }else if($("#current_language").val() == 'zh-hans'){
                 $("#spaname_text").html('-请选择疗程-');
               }else{
                 $("#spaname_text").html('-Please select-');
               } $("#spaname_ul").removeClass('spaname_list');
               $("#spaname_ul").html('');
               if($('.spaname_info_'+type).length){
                 var html2=""
                 var num2=1;
                 $('.spaname_info_'+type).each(function(){
                   var spaname= $(this).val();
                   if(spaname.indexOf("/") != -1){
                     var spanamelist = spaname.split("/");
                     if(num2 == 1){
                       // $("#spaname").val(spanamelist[1]);
                     }
                     html2 += ' <li data-value="'+spanamelist[0]+'">'+spanamelist[1]+'</li>';
                     num2++;
                   }
                 });
                 $("#spaname_ul").html(html2);
                 $('#spaname_ul li').click(function(){
                  $("#spaname_text").addClass('new_select_txt');
                });
               }
             }else if(id == "spaname"){
               $(".Error_Tips_spaname").hide();
             }else if(id == "titlesel"){
               $(".Error_Tips_Title").hide();
             }else if(id == "time"){
               $(".Error_Tips5").hide();
             }else if(id == "gender_value"){
               $(".Error_Tips_Therapist").hide();
             }
           }
         }
       }
     },
     getByClass:function(s,p,t)//使用class获取元素
     {
       var reg=new RegExp('\\b'+s+'\\b');
       var aResult=[];
       var aElement=(p||document).getElementsByTagName(t || '*');

       for(var i=0;i<aElement.length;i++)
       {
         if(reg.test(aElement[i].className))
         {
           aResult.push(aElement[i])
         }
       }
       return aResult;
     },

     setOpts:function(opt) //以下参数可以不设置  //设置参数
     { 
       this.opt={
          TTContainer:'diy_select',//控件的class
          TTDiy_select_input:'diy_select_input',//用于提交表单的class
          TTDiy_select_txt:'diy_select_txt',//diy_select用于显示当前选中内容的容器class
          TTDiy_select_btn:'diy_select_btn',//diy_select的打开按钮
          TTDiv_select_list:'diy_select_list',//要显示的下拉框内容列表class
          TTFcous:'focus'//得到焦点时的class
       }
       for(var a in opt)  //赋值 ,请保持正确,没有准确判断的
       {
         this.opt[a]=opt[a] ? opt[a]:this.opt[a];
       }
     }
  }


  var TTDiy_select=new diy_select({  //参数可选
  TTContainer:'diy_select',//控件的class
  TTDiy_select_input:'diy_select_input',//用于提交表单的class
  TTDiy_select_txt:'diy_select_txt',//diy_select用于显示当前选中内容的容器class
  TTDiy_select_btn:'diy_select_btn',//diy_select的打开按钮
  TTDiv_select_list:'diy_select_list',//要显示的下拉框内容列表class
  TTFcous:'focus'//得到焦点时的class
  });//如同时使用多个时请保持各class一致.


  $('#spatype_text').click(function(){
      var diyselectH = $("#spatype_text").height();
      $("#spatype_ul").css("top",diyselectH + 15);
      $(".diy_select_list").css('border','1px solid #aaa');

  });
  $('#spaname_text').click(function(){
    var diyselectH1 = $("#spaname_text").height();
    $("#spaname_ul").css("top",diyselectH1 + 15);
});
  


$('#time_ul li').click(function(){
  $("#time_text").addClass('new_select_txt');
});
$('#titlesel_ul li').click(function(){
  $("#title_text").addClass('new_select_txt');
});


    // $("#BOOKModal").scroll(function(){
    //     $('.spaDatastep1').addClass('displayblock');   
    // });




  

})(jQuery);
