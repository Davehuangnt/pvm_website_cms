(function($) {
    $(function () {
        var filterList = {
          init: function () {
            // MixItUp plugin
            $('#portfoliolist').mixitup({
              targetSelector: '.portfolio',
              filterSelector: '.filter',
              effects: ['fade'],
              easing: 'snap',
              // call the hover effect
              onMixEnd: filterList.hoverEffect()
            });       
          },
          hoverEffect: function () {
            // Simple parallax effect
            $('#portfoliolist .portfolio').hover(
              function () {
                $(this).find('.label').stop().animate({bottom: 0}, 200, 'easeOutQuad');
              },
              function () {
                $(this).find('.label').stop().animate({bottom: -40}, 200, 'easeInQuad');
              }   
            );        
          }
        };
        // Run the show!
        filterList.init();
      }); 

})(jQuery);

