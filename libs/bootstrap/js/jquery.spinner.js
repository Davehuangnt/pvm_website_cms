(function(a) {
    a.fn.spinner = function(b) {
        return this.each(function() {
            var f = {
                value: 0,
                min: 0,
                max: 0
            };
            var k = a.extend(f, b);
            var j = {
                up: 38,
                down: 40
            };
            var d = a("<div></div>");
            d.addClass("spinner");
            var l = a(this).addClass("value").attr("maxlength", "2").val(k.value).bind("keyup paste change",
            function(o) {
                var p = a(this);
                if (o.keyCode == j.up) {
                    c(1)
                } else {
                    if (o.keyCode == j.down) {
                        c( - 1)
                    } else {
                        if (g(p) != d.data("lastValidValue")) {
                            n(p)
                        }
                    }
                }
            });
            l.wrap(d);
            var h = a('<a class="increase"></a>').click(function() {
                c(1)
            });
            var e = a('<a class="decrease"></a>').click(function() {
                c( - 1)
            });
            m(l);
            d.data("lastValidValue", k.value);
            l.before(e);
            l.after(h);
            function c(o) {
                l.val(g() + o);
                n(l)
            }
            function n(o) {
                clearTimeout(d.data("timeout"));
                var p = m(o);
                if (!i(p)) {
                    l.trigger("update", [o, p])
                }
            }
            function m(o) {
                var q = g();
                if (q <= k.min) {
                    //e.attr("disabled", "disabled")
                    e.attr("disabled",true).css("pointer-events","none");  
                } else {
                    e.removeAttr("disabled").css("pointer-events","auto")
                }
                if (q >= k.max) {
                    //h.attr("disabled", "disabled")
                    h.attr("disabled",true).css("pointer-events","none");  
                } else {
                    h.removeAttr("disabled").css("pointer-events","auto")
                }
                o.toggleClass("invalid", i(q)).toggleClass("passive", q === 0);
                if (i(q)) {
                    var p = setTimeout(function() {
                        l.val(d.data("lastValidValue"));
                        m(o)
                    },
                    500);
                    d.data("timeout", p)
                } else {
                    d.data("lastValidValue", q)
                }
                return q
            }
            function i(o) {
                return isNaN( + o) || o < k.min || o > k.max
            }
            function g(o) {
                o = o || l;
                return parseInt(o.val() || 0, 10)
            }
        })
    }
})(jQuery);